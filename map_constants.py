#configue
SCREEN_WIDTH = 1050
SCREEN_HEIGHT = 1050

#propriété de la map
NB_DETAILS = 8
NB_LINES = 61
NB_COLUMNS = 61
NB_CITIES = 12

#éléments principaux de la map
WALL = "A"
CITY = "Z"
PATH = " "
WILD_GRASS = "E"
GRASS = "R"

#les bordures des chemins
BORDER_LTR = "T"
BORDER_TRB = "Y"
BORDER_LRB = "U"
BORDER_LTB = "I"

BORDER_LR = "O"
BORDER_TB = "P"
BORDER_LT = "Q"
BORDER_TR = "S"
BORDER_BR = "D"
BORDER_BL = "F"

BORDER_T = "G"
BORDER_B = "H"
BORDER_L = "J"
BORDER_R = "K"

#dans les villes (les id doivent se suivrent)
CENTER = 1
SHOP = CENTER+4*1
GYM = CENTER+4*2

HOUSE1 = CENTER+4*3
HOUSE2 = CENTER+4*4

DATA_RANGE = 10000

HOUSES = [HOUSE1, HOUSE2]



#labyrinthe
START_CASE = [1,1]

#les bloques avec des collisions
SOLID_BLOCKS = [WALL, HOUSE1, HOUSE2, SHOP, GYM, CENTER]