import pygame
from pygame.locals import *

from display import *
from generate_map import *
from map_constants import *

#on créée la carte où le joueur va jouer
matrix_enlarges, matrix_reduce, list_intersection, layer_road = generate_map()
x_player = list_intersection[0][0]*8+2
y_player = list_intersection[0][1]*8+2

aventure = [x_player,y_player,matrix_enlarges, matrix_reduce, list_intersection, layer_road]

#boucle principale
running = True
while (running):
	#on affiche la map où le joueur se déplace
	display_map(aventure, "ZOOMED_IN")

	#gestions de tout les event (clics, ...)
	for event in pygame.event.get():
		#si le joueur ferme la fenêtre
		if(event.type == QUIT):
			running = False
			pygame.quit()
		elif(event.type == KEYDOWN):
			carac = event.dict['unicode']
			#on affiche la carte global lorsqu'il appuie sur la touche m
			if(carac == 'm'):
				display_map(aventure, "ZOOMED_OUT")
				
				display_reduce_map = True
				while(display_reduce_map):
					for event_map in pygame.event.get():
						if(event_map.type == pygame.QUIT):
							running = False
							display_reduce_map = False
						if(event_map.type == pygame.MOUSEBUTTONDOWN):
							if(image_red_cross_out_rect.collidepoint(event_map.pos)):
								display_reduce_map = False
			#il décide de décendre d'une case
			if (carac == 's' or event.key == pygame.K_DOWN):
				#on regarde si ce n'est pas un bloc avec des collisions
				if( get_val(aventure[2], aventure[1]+1, aventure[0]) not in SOLID_BLOCKS):
					aventure[1] += 1
			#il décide de monter d'une case
			if (carac == 'z' or event.key == pygame.K_UP):
				if( get_val(aventure[2], aventure[1]-1, aventure[0]) not in SOLID_BLOCKS):
					aventure[1] += -1
			#il décide d'aller à gauche
			if (carac == "q" or event.key == pygame.K_LEFT):
				if( get_val(aventure[2], aventure[1], aventure[0]-1) not in SOLID_BLOCKS):
					aventure[0] += -1
			#il décide d'aller à droite
			if (carac == "d" or event.key == pygame.K_RIGHT):
				if( get_val(aventure[2], aventure[1], aventure[0]+1) not in SOLID_BLOCKS):
					aventure[0] += 1
pygame.quit()