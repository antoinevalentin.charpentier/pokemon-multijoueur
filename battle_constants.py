import random
from model import Move,Pokemon

#Natures et fonctions associées
pokemon_natures = {"Bold":(1,2),
				   "Quirky":(None,None),
	 			   "Brave":(5,1),
	 			   "Calm":(1,4),
	 			   "Quiet":(5,3),
	 			   "Docile":(None,None),
	 			   "Mild":(2,3),
	 			   "Rash":(4,3),
	 			   "Gentle":(2,4),
	 			   "Hardy":(None,None),
	 			   "Jolly":(3,5),
	 			   "Lax":(4,2),
	 			   "Impish":(3,2),
	 			   "Sassy":(5,4),
	 			   "Naughty":(4,1),
	 			   "Modest":(1,3),
	 			   "Naive":(4,5),
	 			   "Hasty":(2,5),
	 			   "Careful":(3,4),
	 			   "Bashful":(None,None),
	 			   "Relaxed":(5,2),
	 			   "Adamant":(3,1),
	 			   "Serious":(None,None),
	 			   "Lonely":(2,1),
	 			   "Timid":(1,5)}

def get_random_nature():
	return random.choice(list(pokemon_natures.keys()))

def apply_nature(pokemon,battle_stats):
	"""
	Applique l'effet de la nature sur des statistiques
	battle_stats est la liste des statisiques
	pokemon est le pokémon qui porte la nature
	"""
	nature = pokemon_natures[pokemon.pokemon_nature]
	if nature != (None,None):
		battle_stats[nature[0]-1] = int(battle_stats[nature[0]-1]*0.9)
		battle_stats[nature[1]-1] = int(battle_stats[nature[1]-1]*1.1)

#Types et fonctions associées
immunities = [("normal", "ghost"), ("electric", "ground"), ("fighting", "ghost"), ("poison","steel"),
			  ("ground", "flying"), ("ghost", "normal"), ("psychic", "dark"), ("dragon", "fairy")]

resistances = [("normal", "rock"), ("normal", "steel"), ("fighting", "flying"), ("fighting", "poison"), ("fighting", "bug"),
			   ("fighting", "psychic"), ("fighting", "fairy"), ("flying", "rock"), ("flying", "steel"), ("flying", "electric"), ("poison", "poison"),
			   ("poison", "ground"), ("poison", "rock"), ("poison", "ghost"), ("ground", "bug"), ("ground", "grass"), ("rock", "fighting"), ("rock", "ground"),
			   ("rock", "steel"), ("bug", "fighting"), ("bug", "flying"), ("bug", "poison"), ("bug", "ghost"), ("bug", "steel"), ("bug", "fire"), ("bug", "fairy"),
			   ("ghost", "dark"), ("steel", "steel"), ("steel", "fire"), ("steel", "water"), ("steel", "electric"), ("fire", "rock"), ("fire", "fire"),
			   ("fire", "water"), ("fire", "dragon"), ("water", "water"), ("water", "grass"), ("water", "dragon"), ("grass", "flying"), ("grass", "poison"),
			   ("grass", "bug"), ("grass", "steel"), ("grass", "fire"), ("grass", "grass"), ("grass", "dragon"), ("electric", "grass"), ("electric", "electric"),
			   ("electric", "dragon"), ("psychic", "steel"), ("psychic", "psychic"), ("ice", "steel"), ("ice", "fire"), ("ice", "water"), ("ice", "ice"), ("dragon", "steel"),
			   ("dark", "fighting"), ("dark", "dark"), ("dark", "fairy"), ("fairy", "poison"), ("fairy", "steel"), ("fairy", "fire")]

weaknesses = [("fighting", "normal"), ("fighting", "rock"), ("fighting", "steel"), ("fighting", "ice"), ("fighting", "dark"), ("flying", "fighting"),
		  	  ("flying", "bug"), ("flying", "grass"), ("poison", "grass"), ("poison", "fairy"), ("ground", "poison"), ("ground", "rock"), ("ground", "steel"),
			  ("ground", "fire"), ("ground", "electric"), ("rock", "flying"), ("rock", "bug"), ("rock", "fire"), ("rock", "ice"), ("bug", "grass"),
			  ("bug", "psychic"), ("bug", "dark"), ("ghost", "ghost"), ("ghost", "psychic"), ("steel", "rock"),("steel", "ice"), ("steel", "fairy"),
			  ("fire", "bug"), ("fire", "steel"), ("fire", "grass"), ("fire", "ice"), ("water", "ground"), ("water", "rock"), ("water", "fire"),
			  ("grass", "ground"), ("grass", "rock"), ("grass", "water"), ("electric", "flying"), ("electric", "water"), ("psychic", "fighting"),
		      ("psychic", "poison"), ("ice", "flying"), ("ice", "ground"), ("ice", "grass"), ("ice", "dragon"), ("dragon", "dragon"), ("dark", "ghost"),
			  ("dark", "psychic"), ("fairy", "fighting"), ("fairy", "dragon"), ("fairy", "dark")]


def get_coefficient_type_to_type(type1,type2):
	"""
	Donne le coefficient d'un type sur un autre
	*0 = pas d'effet, *0,5 = peu efficace,
	*1 = normal et *2 = super efficace
	type1 est le type offensif
	"""
	if (type1,type2) in immunities:
		return 0
	elif (type1,type2) in resistances:
		return 0.5
	elif (type1,type2) in weaknesses:
		return 2
	else:
		return 1

def get_coefficient_move_to_pokemon(move,defender):
	"""
	Retourne le coefficient sur l'attaque d'un pokémon
	sur un pokémon
	"""
	base_coefficient = 1
	pokemon_types = (defender.pokemon.pokemon_type1, defender.pokemon.pokemon_type2)
	for pokemon_type in pokemon_types:
		if pokemon_type != None:
			base_coefficient *= get_coefficient_type_to_type(move.move_type,pokemon_type)
	if base_coefficient == 0:
		print("Pas d'effet")
	elif base_coefficient > 0 and base_coefficient < 1:
		print("Ce n'est pas très efficace")
	elif base_coefficient > 1:
		print("C'est super efficace")
	return base_coefficient
