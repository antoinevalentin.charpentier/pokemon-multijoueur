#Imports
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import func

import random
import pickle
from generate_map import convert_list_road_into_layer

#Création de la base de données
engine = create_engine('sqlite:///test.db')

#Création de la base pour le modèle objet = lien entre python et bd
Base = declarative_base()

#Tables
class Move(Base):
	"""
	Informations sur un move
	"""
	#Nom de la table
	__tablename__ = "move"

	#Attributs = Colonnes de la table
	move_id = Column(Integer, primary_key=True)
	move_name = Column(String)
	move_pp = Column(Integer)
	move_power = Column(Integer)
	move_accuracy = Column(Integer)
	move_damage_class = Column(String)
	move_priority = Column(String)
	move_type = Column(String)
	move_target = Column(String)
	move_status = Column(String)
	move_status_chance = Column(Integer)
	move_crit_chance = Column(Integer)
	move_drain = Column(Integer)
	move_heal = Column(Integer)
	move_flinch = Column(Integer)
	move_max_hits = Column(Integer)
	move_min_hits = Column(Integer)
	move_max_turns = Column(Integer)
	move_min_turns = Column(Integer)
	move_load_turns = Column(Integer)
	move_stat_modify_chance = Column(Integer)
	move_stat_changed = Column(Integer)
	move_stat_amplifier = Column(Integer)

	#Association avec learn
	learn = relationship("Learn", back_populates="move")

	def move_to_move_no_database_link(self):
		return MoveNoDatabaseLink(self.move_id,self.move_name,self.move_pp,self.move_power,self.move_accuracy,self.move_damage_class,self.move_priority,self.move_type,self.move_target
			,self.move_status,self.move_status_chance,self.move_crit_chance,self.move_drain,self.move_heal,self.move_flinch,self.move_max_hits,self.move_min_hits,self.move_max_turns
			,self.move_min_turns,self.move_load_turns,self.move_stat_modify_chance,self.move_stat_changed,self.move_stat_amplifier)

class MoveNoDatabaseLink():
	"""
	Move sans interfacage avec la BD
	"""
	def __init__(self,move_id,move_name,move_pp,move_power,move_accuracy,move_damage_class,move_priority,move_type,move_target,move_status,move_status_chance,move_crit_chance,move_drain,move_heal,move_flinch,move_max_hits,move_min_hits,move_max_turns,move_min_turns,move_load_turns,move_stat_modify_chance,move_stat_changed,move_stat_amplifier):
		self.move_id = move_id
		self.move_name = move_name
		self.move_pp = move_pp
		self.move_power = move_power
		self.move_accuracy = move_accuracy
		self.move_damage_class = move_damage_class
		self.move_priority = move_priority
		self.move_type = move_type
		self.move_target = move_target
		self.move_status = move_status
		self.move_status_chance = move_status_chance
		self.move_crit_chance = move_crit_chance
		self.move_drain = move_drain
		self.move_heal = move_heal
		self.move_flinch = move_flinch
		self.move_max_hits = move_max_hits
		self.move_min_hits = move_min_hits
		self.move_max_turns = move_max_turns
		self.move_min_turns = move_min_turns
		self.move_load_turns = move_load_turns
		self.move_stat_modify_chance = move_stat_modify_chance
		self.move_stat_changed = move_stat_changed
		self.move_stat_amplifier = move_stat_amplifier

	def __repr__(self):
		return self.move_name

class Pokemon(Base):
	"""
	Informations sur un pokémon (type pokédex)
	"""
	#Nom de la table
	__tablename__ = "pokemon"

	#Attributs
	pokemon_id = Column(Integer, primary_key=True)
	pokemon_name = Column(String)
	pokemon_type1 = Column(String)
	pokemon_type2 = Column(String)
	pokemon_ability1 = Column(String)
	pokemon_ability2 = Column(String)
	pokemon_ability3 = Column(String)
	pokemon_evolution_pokemon = Column(String)
	pokemon_evolution_level = Column(Integer)
	pokemon_evolution_item = Column(String)
	pokemon_evolution_happiness = Column(Integer)
	pokemon_base_pv = Column(Integer)
	pokemon_base_attack = Column(Integer)
	pokemon_base_defense = Column(Integer)
	pokemon_base_special_attack = Column(Integer)
	pokemon_base_special_defense = Column(Integer)
	pokemon_base_speed = Column(Integer)
	pokemon_xp_reward = Column(Integer)
	pokemon_xp_curve = Column(String)
	pokemon_base_experience = Column(Integer)

	#Association avec learn
	learn = relationship("Learn", back_populates="pokemon")

	#Association avec team
	team = relationship("Team", back_populates="pokemon")

	#Association avec plan_team
	plan_team = relationship("PlanTeam", back_populates="pokemon")

	#Association avec WildPokemon
	wild_pokemon = relationship("WildPokemon", back_populates="pokemon")

	def pokemon_to_pokemon_no_database_link(self):
		return PokemonNoDatabaseLink(self.pokemon_id,self.pokemon_name,self.pokemon_type1,self.pokemon_type2,self.pokemon_ability1,self.pokemon_ability2,self.pokemon_ability3
			,self.pokemon_evolution_pokemon,self.pokemon_evolution_level,self.pokemon_evolution_item,self.pokemon_evolution_happiness,self.pokemon_base_pv,self.pokemon_base_attack
			,self.pokemon_base_defense,self.pokemon_base_special_attack,self.pokemon_base_special_defense,self.pokemon_base_speed,self.pokemon_xp_reward,self.pokemon_xp_curve,self.pokemon_base_experience)

class PokemonNoDatabaseLink():
	"""
	Pokemon sans interfacage avec la BD
	"""
	def __init__(self,pokemon_id,pokemon_name,pokemon_type1,pokemon_type2,pokemon_ability1,pokemon_ability2,pokemon_ability3,pokemon_evolution_pokemon,pokemon_evolution_level,pokemon_evolution_item,pokemon_evolution_happiness,pokemon_base_pv,pokemon_base_attack,pokemon_base_defense,pokemon_base_special_attack,pokemon_base_special_defense,pokemon_base_speed,pokemon_xp_reward,pokemon_xp_curve,pokemon_base_experience):
		self.pokemon_id = pokemon_id
		self.pokemon_name = pokemon_name
		self.pokemon_type1 = pokemon_type1
		self.pokemon_type2 = pokemon_type2
		self.pokemon_ability1 = pokemon_ability1
		self.pokemon_ability2 = pokemon_ability2
		self.pokemon_ability3 = pokemon_ability3
		self.pokemon_evolution_pokemon = pokemon_evolution_pokemon
		self.pokemon_evolution_level = pokemon_evolution_level
		self.pokemon_evolution_item = pokemon_evolution_item
		self.pokemon_evolution_happiness = pokemon_evolution_happiness
		self.pokemon_base_pv = pokemon_base_pv
		self.pokemon_base_attack = pokemon_base_attack
		self.pokemon_base_defense = pokemon_base_defense
		self.pokemon_base_special_attack = pokemon_base_special_attack
		self.pokemon_base_special_defense = pokemon_base_special_defense
		self.pokemon_base_speed = pokemon_base_speed
		self.pokemon_xp_reward = pokemon_xp_reward
		self.pokemon_xp_curve = pokemon_xp_curve
		self.pokemon_base_experience = pokemon_base_experience

class Learn(Base):
	"""
	Informations sur les moves appris par les pokémons
	"""
	#Nom de la table
	__tablename__ = "learn"

	#Attribut
	level = Column(Integer)

	#Association avec move
	move_id = Column(Integer, ForeignKey('move.move_id'), primary_key=True)
	move = relationship("Move", back_populates="learn")

	#Association avec pokemon
	pokemon_id = Column(Integer, ForeignKey('pokemon.pokemon_id'), primary_key=True)
	pokemon = relationship("Pokemon", back_populates="learn")


class Trainer(Base):
	"""
	Informations sur un joueur
	"""
	#Nom de la table
	__tablename__ = "trainer"

	#Attributs
	trainer_id = Column(Integer, primary_key=True)
	trainer_name = Column(String)

	#Association avec plan
	plan = relationship("Plan", back_populates="trainer")

	#Association avec adventure
	adventure = relationship("Adventure", back_populates="trainer")

	
class Plan(Base):
	"""
	Informations sur la Plan
	"""
	#Nom de la table
	__tablename__ = "plan"	

	#Attributs
	plan_id = Column(Integer, primary_key=True)
	plan_file = Column(String)

	#Association avec trainer
	plan_owner_id = Column(String, ForeignKey('trainer.trainer_id'))
	trainer = relationship("Trainer", back_populates="plan")

	#Association avec adventure
	adventure = relationship("Adventure", back_populates="plan")

	#Association avec road
	road = relationship("Road", back_populates="plan")


class Adventure(Base):
	"""
	Informations sur le joueur d'un plan
	"""
	#Nom de la table
	__tablename__ = "adventure"

	#Attributs
	adventure_id = Column(Integer, primary_key=True)
	adventure_money = Column(Integer)
	adventure_badges = Column(String)
	adventure_coordinate_x = Column(Integer)
	adventure_coordinate_y = Column(Integer)

	#Association avec trainer
	trainer_id = Column(Integer, ForeignKey('trainer.trainer_id'))
	trainer = relationship("Trainer", back_populates="adventure")

	#Association avec plan
	plan_id = Column(Integer, ForeignKey('plan.plan_id'))
	plan = relationship("Plan", back_populates="adventure")

	#Association avec team
	team = relationship("Team", back_populates="adventure")

	#Association avec bag
	bag = relationship("Bag", back_populates="adventure")

	#Association avec battle
	battle = relationship("Battle", back_populates="adventure")

class Team(Base):
	"""
	Informations sur l'équipe d'un joueur dans une plan
	"""
	#Nom de la table
	__tablename__ = "team"

	#Attributs
	team_id = Column(Integer, primary_key=True)
	pokemon_evs = Column(String)
	pokemon_ivs = Column(String)
	#0 non shiny 1 shiny
	pokemon_shiny = Column(Integer)
	pokemon_surname = Column(String)
	pokemon_held_item = Column(String)
	pokemon_level = Column(Integer)
	pokemon_current_xp = Column(Integer)
	pokemon_ability = Column(String)
	pokemon_nature = Column(String)
	pokemon_remaning_hp = Column(Integer)
	pokemon_status = Column(String)
	pokemon_slot = Column(Integer) #Les slots 0 à 5 correspondent à l'équipe, à partir du slot 6 c'est le PC
	pokemon_first_move = Column(Integer)
	pokemon_second_move = Column(Integer)
	pokemon_third_move = Column(Integer)
	pokemon_fourth_move = Column(Integer)
	pokemon_first_move_pp = Column(Integer)
	pokemon_second_move_pp = Column(Integer)
	pokemon_third_move_pp = Column(Integer)
	pokemon_fourth_move_pp = Column(Integer)

	#Association avec pokemon
	pokemon_id = Column(Integer, ForeignKey('pokemon.pokemon_id'))
	pokemon = relationship("Pokemon", back_populates="team")	

	#Association avec trainer
	adventure_id = Column(Integer, ForeignKey('adventure.adventure_id'))
	adventure = relationship("Adventure", back_populates="team")

	def team_to_team_no_database_link(self, session):
		"""
		Retransformer un objet BD en objet non BD
		"""
		team_no_database = TeamNoDatabaseLink(self.team_id,self.pokemon_evs,self.pokemon_ivs,self.pokemon_shiny,self.pokemon_surname,self.pokemon_held_item,self.pokemon_level
		,self.pokemon_current_xp,self.pokemon_ability,self.pokemon_nature,self.pokemon.pokemon_to_pokemon_no_database_link())
		team_no_database.pokemon_remaning_hp = self.pokemon_remaning_hp
		team_no_database.pokemon_status = self.pokemon_status
		team_no_database.pokemon_slot = self.pokemon_slot
		team_no_database.pokemon_first_move = get_move(session,self.pokemon_first_move).move_to_move_no_database_link()
		team_no_database.pokemon_second_move = get_move(session,self.pokemon_second_move).move_to_move_no_database_link()
		team_no_database.pokemon_third_move = get_move(session,self.pokemon_third_move).move_to_move_no_database_link()
		team_no_database.pokemon_fourth_move = get_move(session,self.pokemon_fourth_move).move_to_move_no_database_link()
		team_no_database.pokemon_first_move_pp = self.pokemon_first_move_pp
		team_no_database.pokemon_second_move_pp = self.pokemon_second_move_pp
		team_no_database.pokemon_third_move_pp = self.pokemon_third_move_pp
		team_no_database.pokemon_fourth_move_pp = self.pokemon_fourth_move_pp
		team_no_database.adventure = self.adventure
		return team_no_database

	#Print pour debugging
	def __repr__(self):
		evs = self.pokemon_evs.split(", ")
		for i in range(len(evs)):
			evs[i] = int(evs[i])
		ivs = self.pokemon_ivs.split(", ")
		for i in range(len(ivs)):
			ivs[i] = int(ivs[i])
		max_hp = int(( ( (2*self.pokemon.pokemon_base_pv + ivs[0] + evs[0]/4) * self.pokemon_level ) / 100) + self.pokemon_level + 10)
		return self.pokemon_surname + " - " + str(self.pokemon_remaning_hp) + "/" + str(max_hp) + "PV - Statut : " + str(self.pokemon_status)


class TeamNoDatabaseLink:
	"""
	Classe Team sans interfacage avec la BD
	"""
	def __init__(self, team_id, pokemon_evs, pokemon_ivs, pokemon_shiny, pokemon_surname, pokemon_held_item, pokemon_level, pokemon_current_xp, pokemon_ability, pokemon_nature, pokemon):
		self.team_id = team_id
		self.pokemon_evs = pokemon_evs
		self.pokemon_ivs = pokemon_ivs
		self.pokemon_shiny = pokemon_shiny
		self.pokemon_surname = pokemon_surname
		self.pokemon_held_item = pokemon_held_item
		self.pokemon_level = pokemon_level
		self.pokemon_current_xp = pokemon_current_xp
		self.pokemon_ability = pokemon_ability
		self.pokemon_nature = pokemon_nature
		self.pokemon_remaning_hp = 0
		self.pokemon_status = None
		self.pokemon_slot = -1
		self.pokemon_first_move = None
		self.pokemon_second_move = None
		self.pokemon_third_move = None
		self.pokemon_fourth_move = None
		self.pokemon_first_move_pp = 0
		self.pokemon_second_move_pp = 0
		self.pokemon_third_move_pp = 0
		self.pokemon_fourth_move_pp = 0
		self.pokemon = pokemon
		self.adventure = None

	def team_no_database_link_to_team(self,session,adventure):
		"""
		Interface avec la BD pour retransformer en objet link avec la BD
		"""

		team = get_team(session,self.team_id)

		#Pokémon déja présent dans la BD
		if team != None:
			team.pokemon_level = self.pokemon_level
			team.pokemon_current_xp = self.pokemon_current_xp
			team.pokemon_remaning_hp = self.pokemon_remaning_hp
			team.pokemon_status = self.pokemon_status
			team.pokemon_first_move = self.pokemon_first_move.move_id
			team.pokemon_second_move = self.pokemon_second_move.move_id
			team.pokemon_third_move = self.pokemon_third_move.move_id
			team.pokemon_fourth_move = self.pokemon_fourth_move.move_id
			team.pokemon_first_move_pp = self.pokemon_first_move_pp
			team.pokemon_second_move_pp = self.pokemon_second_move_pp
			team.pokemon_third_move_pp = self.pokemon_third_move_pp
			team.pokemon_fourth_move_pp = self.pokemon_fourth_move_pp
			return team
		
		#Pokémon non présent dans la BD, il faut le créer
		else:
			return Team(team_id=self.team_id,pokemon_evs=self.pokemon_evs,pokemon_ivs=self.pokemon_ivs,pokemon_shiny=self.pokemon_shiny,pokemon_surname=self.pokemon_surname,
				pokemon_held_item=self.pokemon_held_item,pokemon_level=self.pokemon_level,pokemon_current_xp=self.pokemon_current_xp,pokemon_ability=self.pokemon_ability,
				pokemon_nature=self.pokemon_nature,pokemon_remaning_hp=self.pokemon_remaning_hp,pokemon_status=self.pokemon_status,pokemon_slot=self.pokemon_slot,
				pokemon_first_move=self.pokemon_first_move.move_id,pokemon_second_move=self.pokemon_second_move.move_id,pokemon_third_move=self.pokemon_third_move.move_id,
				pokemon_fourth_move=self.pokemon_fourth_move.move_id,pokemon_first_move_pp=self.pokemon_first_move_pp,pokemon_second_move_pp=self.pokemon_second_move_pp,
				pokemon_third_move_pp=self.pokemon_third_move_pp,pokemon_fourth_move_pp=self.pokemon_fourth_move_pp,pokemon=get_pokemon(session,self.pokemon.pokemon_id),adventure=adventure)

	#Print pour debugging
	def __repr__(self):
		evs = self.pokemon_evs.split(", ")
		for i in range(len(evs)):
			evs[i] = int(evs[i])
		ivs = self.pokemon_ivs.split(", ")
		for i in range(len(ivs)):
			ivs[i] = int(ivs[i])
		max_hp = int(( ( (2*self.pokemon.pokemon_base_pv + ivs[0] + evs[0]/4) * self.pokemon_level ) / 100) + self.pokemon_level + 10)
		return self.pokemon_surname + " - " + str(self.pokemon_remaning_hp) + "/" + str(max_hp) + "PV - Statut : " + str(self.pokemon_status) + " Level: "+str(self.pokemon_level)

class Bag(Base):
	"""
	Informations sur le sac d'un joueur dans une plan
	"""
	#Nom de la table
	__tablename__ = "bag"

	#Attributs
	bag_id = Column(Integer, primary_key=True)
	item_name = Column(String, primary_key=True)
	quantity = Column(Integer)

	#Association avec trainer
	adventure_id = Column(Integer, ForeignKey('adventure.adventure_id'))
	adventure = relationship("Adventure", back_populates="bag")

	#Print pour debugging
	def __repr__(self):
		return str(self.quantity) + "- " + self.item_name

class Road(Base):
	"""
	Informations sur les routes
	"""
	#Nom de la table
	__tablename__ = "road"

	#Attributs
	road_id = Column(Integer, primary_key=True)
	road_number = Column(Integer)
	road_list = Column(String)

	#Association avec plan
	plan_id = Column(Integer, ForeignKey('plan.plan_id'))
	plan = relationship("Plan", back_populates="road")

	#Association avec WildPokemon
	wild_pokemon = relationship("WildPokemon", back_populates="road")

	#Association avec PlanTrainer
	plan_trainer = relationship("PlanTrainer", back_populates="road")

class WildPokemon(Base):
	"""
	Informations sur les pokemons d'une zone
	"""
	#Nom de la table
	__tablename__ = "wild_pokemon"

	#Attributs
	wild_pokemon_id = Column(Integer, primary_key=True)
	frequency = Column(Integer)

	#Association avec road
	road_id = Column(Integer, ForeignKey('road.road_id'))
	road = relationship("Road", back_populates="wild_pokemon")

	#Association avec pokemon
	pokemon_id = Column(Integer, ForeignKey('pokemon.pokemon_id'))
	pokemon = relationship("Pokemon", back_populates="wild_pokemon")

class PlanTrainer(Base):
	"""
	Informations sur les dresseurs d'une zone
	"""
	#Nom de la table
	__tablename__ = "plan_trainer"

	#Attributs
	plan_trainer_id = Column(Integer, primary_key=True)
	plan_trainer_x = Column(Integer)
	plan_trainer_y = Column(Integer)

	#Association avec road
	road_id = Column(Integer, ForeignKey('road.road_id'))
	road = relationship("Road", back_populates="plan_trainer")

	#Association avec plan_team
	plan_team = relationship("PlanTeam", back_populates="plan_trainer")

	#Association avec battle
	battle = relationship("Battle", back_populates="plan_trainer")

class PlanTeam(Base):
	"""
	Informations sur l'équipe d'un dresseur dans une map
	"""
	#Nom de la table
	__tablename__ = "plan_team"

	#Attributs
	plan_team_id = Column(Integer, primary_key=True)
	pokemon_evs = Column(String)
	pokemon_ivs = Column(String)
	#0 non shiny 1 shiny
	pokemon_shiny = Column(Integer)
	pokemon_surname = Column(String)
	pokemon_held_item = Column(String)
	pokemon_level = Column(Integer)
	pokemon_ability = Column(String)
	pokemon_nature = Column(String)
	pokemon_slot = Column(Integer)
	pokemon_first_move = Column(Integer)
	pokemon_second_move = Column(Integer)
	pokemon_third_move = Column(Integer)
	pokemon_fourth_move = Column(Integer)

	#Association avec plan_trainer
	plan_trainer_id = Column(Integer, ForeignKey('plan_trainer.plan_trainer_id'))
	plan_trainer = relationship("PlanTrainer", back_populates="plan_team")

	#Association avec pokemon
	pokemon_id = Column(Integer, ForeignKey('pokemon.pokemon_id'))
	pokemon = relationship("Pokemon", back_populates="plan_team")

class Battle(Base):
	"""
	Informations sur les dresseurs qui ont été combattus par un joueur
	"""
	#Nom de la table
	__tablename__ = "battle"

	battle_id = Column(Integer, primary_key=True)

	#Association avec plan_trainer
	plan_trainer_id = Column(Integer, ForeignKey('plan_trainer.plan_trainer_id'))
	plan_trainer = relationship("PlanTrainer", back_populates="battle")

	#Association avec adventure
	adventure_id = Column(Integer, ForeignKey('adventure.adventure_id'))
	adventure = relationship("Adventure", back_populates="battle")	

class City(Base):
	"""
	Informations sur une ville
	"""
	#Nom de la table
	__tablename__ = "city"

	#Attributs
	city_id = Column(Integer, primary_key=True)
	city_name = Column(String)
	city_x_corner = Column(Integer)
	city_y_corner = Column(Integer)
	city_width = Column(Integer)
	city_height = Column(Integer)
	city_x_output = Column(Integer)
	city_y_output = Column(Integer)

	#Association avec city_trainer
	city_team_id = Column(Integer, ForeignKey('city_team.city_team_id'))
	city_team = relationship("CityTeam", back_populates="city")

class CityTeam(Base):
	"""
	Informations sur l'équipe d'un dresseur dans une ville
	"""
	#Nom de la table
	__tablename__ = "city_team"

	#Attributs
	city_team_id = Column(Integer, primary_key=True)
	pokemon_evs = Column(String)
	pokemon_ivs = Column(String)
	#0 non shiny 1 shiny
	pokemon_shiny = Column(Integer)
	pokemon_surname = Column(String)
	pokemon_held_item = Column(String)
	pokemon_level = Column(Integer)
	pokemon_ability = Column(String)
	pokemon_nature = Column(String)
	pokemon_slot = Column(Integer)
	pokemon_first_move = Column(Integer)
	pokemon_second_move = Column(Integer)
	pokemon_third_move = Column(Integer)
	pokemon_fourth_move = Column(Integer)

	#Association avec city
	city = relationship("City", back_populates="city_team")


#Cration des tables qui n'existent pas encore
Base.metadata.create_all(engine)

#PARTIE QUERIES

#Méthodes pour récupérer les id max
def get_new_team_id(session):
	team_id = session.query(func.max(Team.team_id)).scalar()
	if team_id == None:
	    return 0
	return team_id + 1

def get_new_road_id(session):
	road_id = session.query(func.max(Road.road_id)).scalar()
	if road_id == None:
	    return 0
	return road_id + 1

def get_new_plan_id(session):
	plan_id = session.query(func.max(Plan.plan_id)).scalar()
	if plan_id == None:
	    return 0
	return plan_id + 1

def get_new_adventure_id(session):
	adventure_id = session.query(func.max(Adventure.adventure_id)).scalar()
	if adventure_id == None:
	    return 0
	return adventure_id + 1

def get_new_wild_pokemon_id(session):
	wild_pokemon_id = session.query(func.max(WildPokemon.wild_pokemon_id)).scalar()
	if wild_pokemon_id == None:
	    return 0
	return wild_pokemon_id + 1

def get_next_slot(session,adventure):
	"""
	Retourne le prochain slot pour un pokémon
	"""
	slots = session.query(Team.pokemon_slot).filter(Team.adventure_id == adventure.adventure_id).all()
	if len(slots) == 0:
		return 0
	max_slot = None
	for slot in slots:
		if max_slot == None:
			max_slot = slot[0]
		else:
			if slot[0] > max_slot:
				max_slot = slot[0]
	return max_slot + 1

#Méthodes pour récupérer les objets par rapport a leurs id
def get_pokemon(session,pokemon_id):
	return session.query(Pokemon).filter(Pokemon.pokemon_id == pokemon_id).first()

def get_adventure(session,adventure_id):
	return session.query(Adventure).filter(Adventure.adventure_id == adventure_id).first()

def get_team(session,team_id):
	return session.query(Team).filter(Team.team_id == team_id).first()

def get_move(session,move_id):
	return session.query(Move).filter(Move.move_id == move_id).first()

def get_trainer(session,trainer_id):
	return session.query(Trainer).filter(Trainer.trainer_id == trainer_id).first()

#Autres méthodes
def get_abilities(session,pokemon):
	"""
	Récupérer tous les talents possibles d'un pokémon
	"""
	abilities_array = [pokemon.pokemon_ability1,pokemon.pokemon_ability2,pokemon.pokemon_ability3]
	for ability in abilities_array:
		if ability == None:
			abilities_array.remove(ability)
	return abilities_array

def get_adventure_team(session,adventure):
	"""
	Récupère l'équipe d'un joueur
	"""
	team = []
	for pokemon in adventure.team:
		if pokemon.pokemon_slot < 6:
			team.append(pokemon)
	return team

def get_adventure_team_no_database_link(session,adventure):
	"""
	Récupère l'équipe d'un joueur
	"""
	team = []
	for pokemon in adventure.team:
		if pokemon.pokemon_slot < 6:
			team.append(pokemon.team_to_team_no_database_link(session))
	return team

def get_bag_no_database_link(session,adventure):
	bag = []
	for item in adventure.bag:
		bag.append([item.item_name,item.quantity])
	return bag

def adventure_to_dict(session,adventure):
	"""
	Retourne une aventure dans une fomr lisible sans BD
	"""
	return adventure.adventure_id
	
def get_trainer_adventures(session,trainer_id):
	"""
	Retourne toutes les adventures d'un dresseur
	"""
	return get_trainer(session,trainer_id).adventure

def update_bag_items(session, bag, adventure):
	""" 
	Met à jour le sac d'un joueur
	"""
	for item in adventure.bag:
		for item_no_link in bag:
			if item_no_link[0] == item.item_name:
				item.quantity = item_no_link[1]

def make_new_adventure(session,trainer_id):
	"""
	Crée une nouvelle aventure pour un joueur
	"""
	plan = Plan(plan_id=get_new_plan_id(session),plan_file="drtrftguyh")
	plan.trainer = get_trainer(session,trainer_id)
	adventure = Adventure(adventure_id=get_new_adventure_id(session),adventure_money=0,adventure_badges="",adventure_coordinate_x=0,adventure_coordinate_y=0)
	adventure.trainer = get_trainer(session,trainer_id)
	adventure.plan = plan
	session.add(plan)
	session.add(adventure)
	session.commit()
	return adventure

def get_map_matrix(session, adventure):
	"""
	Retourne la map sous forme d'une matrice
	"""

	plan = adventure.plan
	id_map = plan.plan_id

	file = open("matrix_enlarges"+str(id_map)+".map", 'rb')
	matrix_enlarges = pickle.load(file)
	file.close()

	file = open("matrix_reduce"+str(id_map)+".map", 'rb')
	matrix_reduce = pickle.load(file)
	file.close()

	layer_road = convert_list_road_into_layer(get_road_list(session,plan))

	return (matrix_enlarges,matrix_reduce,layer_road,adventure.adventure_coordinate_x,adventure.adventure_coordinate_y)

def insert_new_road(session, road_number, cells, plan):
	"""
	Insert une nouvelle route dans la base de données
	"""
	road = Road(road_id=get_new_road_id(session), road_number=road_number, road_list=cells, plan=plan)
	session.add(road)
	session.commit()

def insert_roads(session, road_list, plan):
	"""
	Insert les routes d'une map dans la base de données
	"""
	print("fgyhujk")
	for i in range(len(road_list)):
		insert_new_road(session,i,str(road_list[i])[1:len(str(road_list[i]))-1],plan)

def get_road_list(session, plan):
	"""
	Récupère la liste des routes d'une map dans la base de données
	"""
	client_road_list = []
	roads = plan.road
	for road in roads:
		client_road_list.append(transform_road_str_to_list(road.road_list))
	return client_road_list

def transform_road_str_to_list(road_str):
	"""
	Transforme une route sous forme d'une chaine de caractères en une route sous forme de liste de tuples
	"""
	road_list = []
	chaine_courante = ""
	cell_x = None
	cell_y = None
	for carac in road_str:
		chaine_courante += carac
		if carac == ",":
			if chaine_courante[1:len(chaine_courante)-1] != "":
				if cell_x == None:
					cell_x = int(chaine_courante[1:len(chaine_courante)-1])
				else:
					cell_x = int(chaine_courante[2:len(chaine_courante)-1])
			chaine_courante = ""
		if carac == ")":
			cell_y = int(chaine_courante[1:len(chaine_courante)-1])
			chaine_courante = ""
			road_list.append((cell_x,cell_y))
	return road_list

def spread_wild_pokemons(session, plan):
	"""
	Répartition des pokémons sur les différentes routes
	"""
	pokemons = session.query(Pokemon).all().copy()
	roads = plan.road.copy()
	for road in roads:
		for i in range(4):
			pokemon = random.choice(pokemons)
			pokemons.remove(pokemon)
			wild_pokemon = WildPokemon(wild_pokemon_id=get_new_wild_pokemon_id(session),frequency=25,road=road,pokemon=pokemon)
			session.add(wild_pokemon)
	session.commit()

#Pour que les autres fichiers puissent commit
def commit_changes(session):
	session.commit()