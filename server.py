import socket
from _thread import *
import pickle
from model import *
from utils import *
from generate_map import generate_map,convert_list_road_into_layer
from map_constants import *

#Session Globale
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)

class Server:
	"""
	Classe Server
	Contient la socket serveur
	Fait les accès nécéssaires en BD pour répondre aux clients (via des threads)
	"""
	
	def __init__(self):
		"""
		Initialisation des attributs du serveur
		Stockage BD
		"""
		self._server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self._clients = {}

	def add_client(self, client, adventure):
		"""
		Ajoute un client à l'index des clients
		"""
		self._clients[client] = adventure

	def remove_client(self, client):
		"""
		Enlève un client à l'index des clients
		"""
		del self._clients[client]

	def get_client(self, client):
		"""
		Retourne les informations indéxées sur le client (l'aventure)
		"""
		return self._clients[client]

	def start(self, address, port):
		"""
		Démarrage du serveur
		"""
		#Initialisation et bind de la socket
		self._server_socket.bind((address, port))
		print("Serveur démarré")

		run = True

		while(run):
			#On attend de recevoir des nouvelles connexions
			self._server_socket.listen(5)
			#On crée un thread par nouvelle connection
			conn, address = self._server_socket.accept()
			start_new_thread(threaded_client,(self,conn,address,))

	def stop(self):
		"""
		Fermeture du serveur
		"""
		self._server_socket.close()

	#A partir de maintenant ce sont les méthodes réponses du serveur
	#Elles sont appelées par le client et répondent à un profil bien précis
	#Elles doivent prendre en paramètre la session client, la socket client ainsi
	#qu'une liste qui contient tous les paramètres que le client donne au serveur
	#Elles doivent aussi obligatoirement retourner une valeure (qui sera envoyée au client)

	def join_adventure(self, session, client, params):
		"""
		Demande du client de rejoindre une partie
		Retourne la map
		"""

		adventure_id = params[0]
		adventure = get_adventure(session,adventure_id)
		self.add_client(client, adventure)
		return get_map_matrix(session,adventure)

	def create_adventure(self, session, client, params):
		"""
		Demande du client de créer une nouvelle aventure et donc une nouvelle map
		"""

		trainer_id = params[0]

		adventure = make_new_adventure(session,trainer_id)

		self.add_client(client,adventure)

		matrix_enlarges, matrix_reduce, list_intersection, layer_road, list_road = generate_map()

		adventure.adventure_coordinate_x = list_intersection[0][0]*NB_DETAILS+NB_DETAILS//2
		adventure.adventure_coordinate_y = list_intersection[0][1]*NB_DETAILS+NB_DETAILS//2

		id_map = adventure.plan.plan_id

		file = open("matrix_enlarges"+str(id_map)+".map", 'wb')
		pickle.dump(matrix_enlarges, file)
		file.close()

		file = open("matrix_reduce"+str(id_map)+".map", 'wb')
		pickle.dump(matrix_reduce, file)
		file.close()

		insert_roads(session, list_road, adventure.plan)
		spread_wild_pokemons(session, adventure.plan)

		return get_map_matrix(session, adventure)

	def join_map(self, session, client, params):
		"""
		Demande du client pour reçevoir son équipe une fois qu'il a rejoint une carte
		Retourne la map
		"""
		return [get_adventure_team_no_database_link(session,self.get_client(client)),get_bag_no_database_link(session,self.get_client(client))]

	def quit(self, session, client, params):
		"""
		Demande du client de quitter une partie
		"""
		self.remove_client(client)

	def give_position(self, session, client, params):
		"""
		Demande du client pour récupérer les positions des autres
		joueurs tout en donnant sa propre position
		"""
		x = params[0]
		y = params[1]
		self.get_client(client).adventure_coordinate_x = x
		self.get_client(client).adventure_coordinate_y = y
		session.add(self.get_client(client))
		session.commit()
		map_clients = []
		for map_client in self.get_map_clients(client):
			map_clients.append((map_client.adventure_coordinate_x,map_client.adventure_coordinate_y))
		return map_clients

	def get_map_clients(self, client):
		"""
		Donne tous les clients connectés sur une map
		"""
		map_clients = []
		for adventure in self._clients.values():
			if self.get_client(client).plan_id == adventure.plan_id:
				map_clients.append(adventure)
		return map_clients

	def wild_battle(self, session, client, params):
		"""
		Demande du client pour rencontrer un pokémon sauvage sur une route donnée
		"""
		road = params[0]
		ennemy_team = [generate_team_pokemon(session,get_pokemon(session,random.randint(1,150)))]
		return ennemy_team

	def capture_pokemon(self, session, client, params):
		"""
		Demande du client pour mettre à jour son équipe lorsqu'il capture un pokémon
		"""
		team = params[0]
		pokemon_captured = params[1]
		pokemon_captured.pokemon_slot = get_next_slot(session,self.get_client(client))
		pokemon_captured = pokemon_captured.team_no_database_link_to_team(session,self.get_client(client))
		add_pokemon_to_team(session,pokemon_captured,self.get_client(client))
		return get_adventure_team_no_database_link(session,self.get_client(client))

	def trainer_battle(self, session, client, params):
		"""
		Demande du client pour rencontrer un dresseur sur une route donnée
		"""
		road = params[0]
		ennemy_team = [generate_team_pokemon(session,get_pokemon(session,random.randint(1,150))),generate_team_pokemon(session,get_pokemon(session,random.randint(1,150))),
		generate_team_pokemon(session,get_pokemon(session,random.randint(1,150))),generate_team_pokemon(session,get_pokemon(session,random.randint(1,150)))]
		return ennemy_team

	#TODO
	def gym_battle(self, session, client, params):
		"""
		Demande du client pour rencontrer un champion dans une ville
		"""
		road = params[0]
		pass

	#TODO
	def pvp_battle(self, session, client,params):
		"""
		Demande du client pour lancer une bataille contre un autre joueur
		"""
		ennemy_adventure = params[0]
		pass

	def refresh_team(self, session, client, params):
		"""
		Demande du client pour synchroniser son équipe par rapport au serveur
		Retourne l'équipe synchronisée
		"""
		team = params[0]
		bag = params[1]
		for team_pokemon in team:
			team_linked = team_pokemon.team_no_database_link_to_team(session,self.get_client(client))
			session.add(team_linked)
		update_bag_items(session, bag, self.get_client(client))
		commit_changes(session)
		return [get_adventure_team_no_database_link(session,self.get_client(client)),get_bag_no_database_link(session,self.get_client(client))]


def get_adventures(session,trainer_id):
	"""
	Donne la liste des aventures d'un dresseur sous forme d'une liste de dictionnaire pickable
	"""
	adventures_pickable = []
	adventures_list = get_trainer_adventures(session,trainer_id)
	for adventure in adventures_list:
		adventures_pickable.append(adventure_to_dict(session,adventure))
	return adventures_pickable

def threaded_client(server, client, address):
	"""
	#1 thread pour communiquer avec 1 client = 1 socket
	"""

	#Création de la session client
	session = Session()

	#Réponse du serveur au client lors de la connexion
	print("Un client vient de se connecter avec pour adresse : ")
	print(address)

	#On récupère le message initial du client
	data = client.recv(2048)
	data = data.decode("utf8")

	#On répond au client en lui donnant ses informations stockées dans la BD
	data_parsed = pickle.dumps(get_adventures(session,data))
	client.send(data_parsed)

	#A partir de maintenant, la communication peut se faire entre le client et le serveur
	run = True
	while run:
		try:
			#Le serveur reçoit une requête du client
			#Méthode bloquante
			data = client.recv(65536)

			#On la décode
			#data est formé de la manière suivante : (fonction,liste_paramètres)
			data = pickle.loads(data)
			print(data)

			#On répond au client en appellant la bonne fonction avec les bons paramètres
			return_value = getattr(server, data[0])(session,client,data[1])

			if return_value != None:
				#On encode la réponse
				return_value_parsed = pickle.dumps(return_value)

				#On envoie la réponse au client
				client.send(return_value_parsed)
			else:
				run = False

		#Si le client se déconnecte
		except Exception as e:
			print(e)
			run = False

	#On arrête la communication avec ce client pour libérer des ressources et terminer le thread
	print("Le client s'est déconnecté")
	#On enlève le client de la liste des clients connectés (dans le cas d'un crash)
	if client in server._clients.keys():
		server.remove_client(client)
	#On ferme la socket client
	client.close()
	#Très important on ferme la session pour pouvoir se reconnecter au serveur plus tard
	session.close()

s = Server()
s.start("localhost",555)