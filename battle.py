from battle_constants import *
from model import *
from utils import *

def engage_battle(adventure, opponent_team, team, bag, wild):
	"""
	Engage une bataille entre un joueur et une équipe de pokémons adverse
	Un pokémon sauvage est considéré comme une équipe composé de 1 pokémon
	"""

	#Si on capture un pokémon durant le combat
	pokemon_captured = None

	#Utilisé pour gérer la fin du combat
	run = True

	#Récupérer l'équipe de l'utilisateur
	user_team = team

	#Récupérer les pokémons sur le terrain pour le premier tour
	user_pokemon = first_alive_pokemon(user_team)
	opponent_pokemon = first_alive_pokemon(opponent_team)

	#Générer leurs statistiques
	user_pokemon_stats = initialise_stats(user_pokemon)
	opponent_pokemon_stats = initialise_stats(opponent_pokemon)

	#Générer les boosts et baisses de stats
	user_pokemon_modifiers = [0]*7
	opponent_pokemon_modifiers = [0]*7

	while(run):
		
		print("Pokemon allié : ")
		print(user_pokemon)
		print(user_pokemon_modifiers)
		print(user_pokemon_stats)
		print(get_stats_changed(user_pokemon_stats, user_pokemon_modifiers, user_pokemon))

		print("Pokemon adverse : ")
		print(opponent_pokemon)
		print(opponent_pokemon_modifiers)
		print(opponent_pokemon_stats)
		print(get_stats_changed(opponent_pokemon_stats, opponent_pokemon_modifiers, opponent_pokemon))

		choice = int(input("Faites votre choix : 1-Attaquer 2-Pokémon 3-Sac 4-Fuir\n"))

		#Attaquer
		if(choice == 1):
			#Récupérer les statistiques modifiées par les boosts et baisses
			user_stats_after_modifiers = get_stats_changed(user_pokemon_stats, user_pokemon_modifiers, user_pokemon)
			opponent_stats_after_modifiers = get_stats_changed(opponent_pokemon_stats, opponent_pokemon_modifiers,opponent_pokemon)

			#Récupérer les moves choisis
			user_move_choice = user_move_pick(user_pokemon)
			opponent_move_choice = opponent_move_pick(opponent_pokemon)

			#Comparaisons de priorité sur les moves
			if user_move_choice.move_priority > opponent_move_choice.move_priority:
				first_attacker = "user"
			elif user_move_choice.move_priority < opponent_move_choice.move_priority:
				first_attacker = "ennemy"
			else:
				#Comparaisons de vitesses pour savoir qui attaque en premier
				#L'utilisateur attaque en premier
				if user_stats_after_modifiers[4] > opponent_stats_after_modifiers[4]:
					first_attacker = "user"
				#Si l'ennemi est plus rapide
				elif user_stats_after_modifiers[4] < opponent_stats_after_modifiers[4]:
					first_attacker = "ennemy"
				#Si il y a speed tail
				else:
					if random.randrange(0,1) == 0:
						first_attacker = "user"
					else:
						first_attacker = "ennemy"

			#On fait attaquer chaque pokémon dans le bon ordre
			if first_attacker == "user":
				apply_status_effects_before_move(user_pokemon)
				attacker_attack_defender(user_move_choice,user_pokemon,opponent_pokemon,user_pokemon_stats,opponent_pokemon_stats,user_pokemon_modifiers,opponent_pokemon_modifiers)
				if user_pokemon.pokemon_remaning_hp > 0 and opponent_pokemon.pokemon_remaning_hp > 0:
					apply_status_effects_before_move(opponent_pokemon)
					attacker_attack_defender(opponent_move_choice,opponent_pokemon,user_pokemon,opponent_pokemon_stats,user_pokemon_stats,opponent_pokemon_modifiers,user_pokemon_modifiers)
			else:
				apply_status_effects_before_move(opponent_pokemon)
				attacker_attack_defender(opponent_move_choice,opponent_pokemon,user_pokemon,opponent_pokemon_stats,user_pokemon_stats,opponent_pokemon_modifiers,user_pokemon_modifiers)
				if user_pokemon.pokemon_remaning_hp > 0 and opponent_pokemon.pokemon_remaning_hp > 0:
					apply_status_effects_before_move(user_pokemon)
					attacker_attack_defender(user_move_choice,user_pokemon,opponent_pokemon,user_pokemon_stats,opponent_pokemon_stats,user_pokemon_modifiers,opponent_pokemon_modifiers)

			#On re-regarde s'il y a des pokémons K.O
			if user_pokemon.pokemon_remaning_hp == 0:
				user_pokemon_modifiers = [0]*7
				user_pokemon.pokemon_status = None
				if is_alive(user_team):
					user_pokemon = first_alive_pokemon(user_team)
					user_pokemon_stats = initialise_stats(user_pokemon)

			if opponent_pokemon.pokemon_remaning_hp == 0:
				level_up(user_pokemon,opponent_pokemon)
				user_pokemon_stats = initialise_stats(user_pokemon)
				opponent_pokemon_modifiers = [0]*7
				opponent_pokemon.pokemon_status = None
				if is_alive(opponent_team):
					opponent_pokemon = first_alive_pokemon(opponent_team)
					opponent_pokemon_stats = initialise_stats(opponent_pokemon)

			#On applique les status (poison, brûlure, trap)
			apply_status_effects_end_turn(user_pokemon,opponent_pokemon)

			#On re-regarde s'il y a des pokémons K.O
			if user_pokemon.pokemon_remaning_hp == 0:
				user_pokemon.pokemon_status = None
				user_pokemon_modifiers = [0]*7
				if is_alive(user_team):
					user_pokemon = first_alive_pokemon(user_team)
					user_pokemon_stats = initialise_stats(user_pokemon)

			if opponent_pokemon.pokemon_remaning_hp == 0:
				level_up(user_pokemon,opponent_pokemon)
				user_pokemon_stats = initialise_stats(user_pokemon)
				opponent_pokemon.pokemon_status = None
				opponent_pokemon_modifiers = [0]*7
				if is_alive(opponent_team):
					opponent_pokemon = first_alive_pokemon(opponent_team)
					opponent_pokemon_stats = initialise_stats(opponent_pokemon)
					
		#Menu pokémon
		elif(choice == 2):

			#Choix du swich-in
			user_pokemon = choose_swich_pokemon(user_team,user_pokemon)

			#Changement des stats
			user_pokemon_stats = initialise_stats(user_pokemon)
			user_pokemon_modifiers = [0]*7

			#Récupération des stats
			user_stats_after_modifiers = get_stats_changed(user_pokemon_stats, user_pokemon_modifiers, user_pokemon)
			opponent_stats_after_modifiers = get_stats_changed(opponent_pokemon_stats, opponent_pokemon_modifiers,opponent_pokemon)

			#Récupérer les moves choisis
			opponent_move_choice = opponent_move_pick(opponent_pokemon)

			#On fait attaquer l'ennemi
			apply_status_effects_before_move(opponent_pokemon)
			attacker_attack_defender(opponent_move_choice,opponent_pokemon,user_pokemon,opponent_pokemon_stats,user_pokemon_stats,opponent_pokemon_modifiers,user_pokemon_modifiers)

			#On re-regarde s'il y a des pokémons K.O
			if user_pokemon.pokemon_remaning_hp == 0:
				user_pokemon.pokemon_status = None
				user_pokemon_modifiers = [0]*7
				if is_alive(user_team):
					user_pokemon = first_alive_pokemon(user_team)
					user_pokemon_stats = initialise_stats(user_pokemon)

			if opponent_pokemon.pokemon_remaning_hp == 0:
				level_up(user_pokemon,opponent_pokemon)
				user_pokemon_stats = initialise_stats(user_pokemon)
				opponent_pokemon.pokemon_status = None
				opponent_pokemon_modifiers = [0]*7
				if is_alive(opponent_team):
					opponent_pokemon = first_alive_pokemon(opponent_team)
					opponent_pokemon_stats = initialise_stats(opponent_pokemon)

			#On applique les status (poison, brûlure, trap)
			apply_status_effects_end_turn(user_pokemon,opponent_pokemon)

			#On re-regarde s'il y a des pokémons K.O
			if user_pokemon.pokemon_remaning_hp == 0:
				user_pokemon.pokemon_status = None
				user_pokemon_modifiers = [0]*7
				if is_alive(user_team):
					user_pokemon = first_alive_pokemon(user_team)
					user_pokemon_stats = initialise_stats(user_pokemon)

			if opponent_pokemon.pokemon_remaning_hp == 0:
				level_up(user_pokemon,opponent_pokemon)
				user_pokemon_stats = initialise_stats(user_pokemon)
				opponent_pokemon.pokemon_status = None
				opponent_pokemon_modifiers = [0]*7
				if is_alive(opponent_team):
					opponent_pokemon = first_alive_pokemon(opponent_team)
					opponent_pokemon_stats = initialise_stats(opponent_pokemon)


		#Sac	
		elif(choice == 3):
			
			#Choix de l'item swich-in
			item_used = choose_battle_item(bag)

			#Utilisation de l'item
			pokemon_captured = use_battle_item(item_used, user_pokemon, opponent_pokemon, adventure, wild)

			if not pokemon_captured:

				#Récupération des stats
				user_stats_after_modifiers = get_stats_changed(user_pokemon_stats, user_pokemon_modifiers, user_pokemon)
				opponent_stats_after_modifiers = get_stats_changed(opponent_pokemon_stats, opponent_pokemon_modifiers,opponent_pokemon)

				#Récupérer les moves choisis
				opponent_move_choice = opponent_move_pick(opponent_pokemon)

				#On fait attaquer l'ennemi
				apply_status_effects_before_move(opponent_pokemon)
				attacker_attack_defender(opponent_move_choice,opponent_pokemon,user_pokemon,opponent_pokemon_stats,user_pokemon_stats,opponent_pokemon_modifiers,user_pokemon_modifiers)

				#On re-regarde s'il y a des pokémons K.O
				if user_pokemon.pokemon_remaning_hp == 0:
					user_pokemon.pokemon_status = None
					user_pokemon_modifiers = [0]*7
					if is_alive(user_team):
						user_pokemon = first_alive_pokemon(user_team)
						user_pokemon_stats = initialise_stats(user_pokemon)

				if opponent_pokemon.pokemon_remaning_hp == 0:
					level_up(user_pokemon,opponent_pokemon)
					user_pokemon_stats = initialise_stats(user_pokemon)
					opponent_pokemon.pokemon_status = None
					opponent_pokemon_modifiers = [0]*7
					if is_alive(opponent_team):
						opponent_pokemon = first_alive_pokemon(opponent_team)
						opponent_pokemon_stats = initialise_stats(opponent_pokemon)

				#On applique les status (poison, brûlure, trap)
				apply_status_effects_end_turn(user_pokemon,opponent_pokemon)

				#On re-regarde s'il y a des pokémons K.O
				if user_pokemon.pokemon_remaning_hp == 0:
					user_pokemon.pokemon_status = None
					user_pokemon_modifiers = [0]*7
					if is_alive(user_team):
						user_pokemon = first_alive_pokemon(user_team)
						user_pokemon_stats = initialise_stats(user_pokemon)

				if opponent_pokemon.pokemon_remaning_hp == 0:
					level_up(user_pokemon,opponent_pokemon)
					user_pokemon_stats = initialise_stats(user_pokemon)
					opponent_pokemon.pokemon_status = None
					opponent_pokemon_modifiers = [0]*7
					if is_alive(opponent_team):
						opponent_pokemon = first_alive_pokemon(opponent_team)
						opponent_pokemon_stats = initialise_stats(opponent_pokemon)
			else:
				run = False

		#Fuite	
		elif(choice == 4):
			if wild:
				run = False
			else:
				print("Vous ne pouvez pas fuir d'un combat de dresseurs")

		run = run and is_alive(user_team) and is_alive(opponent_team)

	if not is_alive(user_team):
		print("Vous avez perdu")
	else:
		print("Vous avez gagné")
	return pokemon_captured