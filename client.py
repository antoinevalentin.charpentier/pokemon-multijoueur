import socket
import pickle
import random
from battle import *
from utils import *
import pygame
from pygame.locals import *

from display import *
from generate_map import *
from map_constants import *

#Initialisations

#Création de la socket client
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Equipe du joueur
team = [] 

#Sac du joueur
bag = []

#Joueurs de la map à afficher
players = []

#Clock poir réguler le nombre de FPS
clock = pygame.time.Clock()

try:
    #Récupérer le trainer_id
    trainer_id = input("Donnez votre indentifiant de dresseur")

    #Lancement de la communication
    client.connect(("localhost", 555))

    #On envoie au serveur les informations nécéssaires a l'indentification client = trainer_id
    data = trainer_id
    data_parsed = data.encode("utf8")
    client.send(data_parsed)

    #Réponse du serveur pour accepter la connexion
    data_parsed = client.recv(2048)
    data = pickle.loads(data_parsed)

    #Réponse du serveur : les aventures du joueur
    print(data)

    #Ici le joueur choisit son aventure
    choosen_adventure = data[0]
    """
    #BLOC POUR CREER UNE AVENTURE (POUR PLUS TARD)
    data = ("create_adventure",[int(trainer_id)])

    #Transformation de la requête pour pouvoir l'envoyer
    data_parsed = pickle.dumps(data)

    #On envoie au serveur
    client.send(data_parsed)

    #On recoit le reponse du serveur
    #Grosse taille car grosses requêtes
    data_parsed = client.recv(1048576)

    #On la décode
    data = pickle.loads(data_parsed)

    #On récupère les 3 matrices (grande, petite et routes)
    print(data)

    matrix_enlarges = data[0]
    matrix_reduce = data[1]
    layer_road = data[2]
    x_player = data[3]
    y_player = data[4]
    """
    #On récupère la map ou le joueur va jouer
    #TEMPO ICI : 1 AVENTURE PAR DRESSEUR DONC L'ID DE L'AVENTURE EST AUSSI CELUI DU DRESSEUR 
    #Ex : joueur 1 rejoint l'aventure 1 et joueur 2 l'aventure 2
    data = ("join_adventure",[9])#[int(trainer_id)])

    #Transformation de la requête pour pouvoir l'envoyer
    data_parsed = pickle.dumps(data)

    #On envoie au serveur
    client.send(data_parsed)

    #On recoit le reponse du serveur
    #Grosse taille car grosses requêtes
    data_parsed = client.recv(1048576)

    #On la décode
    data = pickle.loads(data_parsed)

    #On récupère les 3 matrices (grande, petite et routes)
    print(data)

    matrix_enlarges = data[0]
    matrix_reduce = data[1]
    layer_road = data[2]
    x_player = data[3]
    y_player = data[4]

    #On laisse la liste des intersections vide pour l'instant, il faudra la remplir plus tard
    aventure = [x_player,y_player,matrix_enlarges, matrix_reduce, [], layer_road]

    #On affiche la map une première fois
    display_map(aventure, "ZOOMED_IN", players)

    #On demande notre équipe au serveur, car coté serveur le client est associé à une aventure
    data = ("join_map",[])

    #Transformation de la requête pour pouvoir l'envoyer
    data_parsed = pickle.dumps(data)

    #On envoie au serveur
    client.send(data_parsed)

    #On recoit le reponse du serveur
    data_parsed = client.recv(65536)

    #On la décode
    data = pickle.loads(data_parsed)

    #On récupère notre équipe et le sac
    team = data[0]
    bag = data[1]
    print(data)

    #Boucle principale du jeu
    running = True
    while (running):

        #Le jeu tourne a 10 FPS (pour l'instant)
        clock.tick(10)

        #On donne notre position au serveur
        data = ("give_position",(aventure[0],aventure[1]))

        #Transformation de la requête pour pouvoir l'envoyer
        data_parsed = pickle.dumps(data)

        #On envoie au serveur
        client.send(data_parsed)

        #On recoit le reponse du serveur
        data_parsed = client.recv(65536)

        #On la décode
        data = pickle.loads(data_parsed)

        #On reçoit les positions de tous les autres joueurs
        print(data)
        players = data

        #On affiche la map où le joueur se déplace
        display_map(aventure, "ZOOMED_IN", players)

        #gestions de tout les event (clics, ...)
        for event in pygame.event.get():
            #si le joueur ferme la fenêtre
            if(event.type == QUIT):
                running = False
                pygame.quit()
            elif(event.type == KEYDOWN):
                carac = event.dict['unicode']
                #on affiche la carte globale lorsqu'il appuie sur la touche m
                if(carac == 'm'):
                    display_map(aventure, "ZOOMED_OUT", players)
                    display_reduce_map = True
                    while(display_reduce_map):
                        for event_map in pygame.event.get():
                            if(event_map.type == pygame.QUIT):
                                running = False
                                display_reduce_map = False
                            if(event_map.type == pygame.MOUSEBUTTONDOWN):
                                if(image_red_cross_out_rect.collidepoint(event_map.pos)):
                                    display_reduce_map = False
                #il décide de décendre d'une case
                if (carac == 's' or event.key == pygame.K_DOWN):
                    #on regarde si ce n'est pas un bloc avec des collisions
                    if( get_val(aventure[2], aventure[1]+1, aventure[0]) not in SOLID_BLOCKS):
                        aventure[1] += 1
                #il décide de monter d'une case
                if (carac == 'z' or event.key == pygame.K_UP):
                    if( get_val(aventure[2], aventure[1]-1, aventure[0]) not in SOLID_BLOCKS):
                        aventure[1] += -1
                #il décide d'aller à gauche
                if (carac == "q" or event.key == pygame.K_LEFT):
                    if( get_val(aventure[2], aventure[1], aventure[0]-1) not in SOLID_BLOCKS):
                        aventure[0] += -1
                #il décide d'aller à droite
                if (carac == "d" or event.key == pygame.K_RIGHT):
                    if( get_val(aventure[2], aventure[1], aventure[0]+1) not in SOLID_BLOCKS):
                        aventure[0] += 1

                #Si le joueur marche dans les hautes herbes, on lance un combat contre un pokémon sauvage
                if get_val(aventure[2], aventure[1], aventure[0]) == WILD_GRASS and random.randint(1,10) == 1:

                    #Requete est un tuple
                    data = ("wild_battle",[get_val(layer_road, aventure[1]//NB_DETAILS, aventure[0]//NB_DETAILS)])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)

                    #On recoit le reponse du serveur
                    data_parsed = client.recv(65536)

                    #On la décode
                    data = pickle.loads(data_parsed)

                    #On récupère le pokémon sauvage
                    print(data)
                    pokemon_captured = None

                    try:
                        pokemon_captured = engage_battle(1, data, team, bag, wild=True)
                    except Exception as e:
                        print("Le jeu a rencontré une erreur :")
                        print(e)

                    print(pokemon_captured)

                    #Si on a capturé le pokémon, on l'ajoute à l'équipe
                    if pokemon_captured:

                        data = ("capture_pokemon",[team,pokemon_captured])

                        #Transformation de la requête pour pouvoir l'envoyer
                        data_parsed = pickle.dumps(data)

                        #On envoie au serveur
                        client.send(data_parsed)

                        #On recoit le reponse du serveur
                        data_parsed = client.recv(65536)

                        #On la décode
                        data = pickle.loads(data_parsed)

                        #On récupère notre nouvelle équipe
                        print(data)
                        team = data

                    #Dans tous les cas, on met à jour notre équipe et notre sac
                    data = ("refresh_team",[team,bag])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)

                    #On recoit le reponse du serveur
                    data_parsed = client.recv(65536)

                    #On la décode
                    data = pickle.loads(data_parsed)
                    team = data[0]
                    bag = data[1]
                    print(data)

                #A PARTIR DE MAINTENANT LE RESTE DES METHODES SONT DES TESTS
                #EN APPUYANT SUR LES TOUCHES DU PAVE NUMERIQUE         

                if(carac == '1'):
                    #Requete est un tuple
                    data = ("trainer_battle",[1])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)

                    #On recoit le reponse du serveur
                    data_parsed = client.recv(65536)

                    #On la décode
                    data = pickle.loads(data_parsed)

                    #on affiche le message reçu dans la console
                    print(data)

                    try:
                        engage_battle(1, data, team, bag, wild=False)
                    except Exception as e:
                        print("Le jeu a rencontré une erreur :")
                        print(e)

                    #Requete est un tuple
                    data = ("refresh_team",[team,bag])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)

                    #On recoit le reponse du serveur
                    data_parsed = client.recv(65536)

                    #On la décode
                    data = pickle.loads(data_parsed)
                    team = data[0]
                    bag = data[1]

                    #on affiche le message reçu dans la console
                    print(data)

                if(carac == '2'):
                    heal_team(team)

                    #Requete est un tuple
                    data = ("refresh_team",[team,bag])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)

                    #On recoit le reponse du serveur
                    data_parsed = client.recv(65536)

                    #On la décode
                    data = pickle.loads(data_parsed)
                    team = data[0]
                    bag = data[1]

                    #on affiche le message reçu dans la console
                    print(data)

                if(carac == '3'):
                    #Requete est un tuple
                    data = ("quit",[1])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)
                    running = False

                #CREATE NEW ADVENTURE PAS ENCORE FONCTIONNEL
                if(carac == '4'):
                    #Le serveur à juste besoin du trainer_id
                    data = ("create_new_adventure",[1])

                    #Transformation de la requête pour pouvoir l'envoyer
                    data_parsed = pickle.dumps(data)

                    #On envoie au serveur
                    client.send(data_parsed)

                    #On recoit le reponse du serveur
                    data_parsed = client.recv(65536)

                    #On la décode
                    data = pickle.loads(data_parsed)

                    #on affiche le message reçu dans la console
                    print(data)

except ConnectionRefusedError:
    print("Le serveur n'est pas démarré")
except Exception as e:
    print("Connexion au serveur échouée")
    print(e)

#Lorsqu'on sort de la boule principale, on peut quitter le jeu
pygame.quit()