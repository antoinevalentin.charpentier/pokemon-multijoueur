from map_constants import *
from city import *
import random
import math

def matrix(nb_lines,nb_columns,default_value = WALL):
	"""
	Crée une matrice vide
	"""
	values=[]
	for i in range(nb_lines):
		#on débute une nouvelle ligne
		line=[]
		#on parcours toute les colonnes
		for j in range(nb_columns):
			#on rajoute toute les valeurs des colonnes présente sur la ligne
			line.append(default_value)
		#on rajoute une ligne dans la matrice
		values.append(line)

	return {"nb_lines":nb_lines,
			"nb_columns":nb_columns,
			"values":values}


def get_val(matrix,line,column):
	"""
	Donne la valeur de la matrice a l'indice (ligne,colonne)
	"""
	return matrix["values"][line][column]

def set_val(matrix,line,column,value):
	"""
	Met la valeur de la matrice à l'indice (ligne,colonne) à valeur
	"""
	matrix["values"][line][column] = value

def get_nb_lines(matrix):
	"""
	Donne le nombre de lignes de la matrice
	"""
	return matrix["nb_lines"]

def get_nb_columns(matrix):
	"""
	Donne le nombre de colonnes de la matrice
	"""
	return matrix["nb_columns"]

def print_matrix(matrix):
	"""
	Affiche une Matrice
	"""
	for j in range(get_nb_lines(matrix)):	
		line=""
		for i in range(get_nb_columns(matrix)):
			line += str(get_val(matrix,j,i))
			line += "-"
		print(line)

def place_cities(matrix):
	"""
	Place les villes de manière aléatoire et de taille aléatoire
	"""
	#X - Y - hauteur en partant du haut à gauche - largeur
	list_cities = []

	while NB_CITIES > len(list_cities):
		#Recours à un booléen pour s'assurer que deux villes ne se chevauche pas
		overlap = False

		#on modifie les dimension de la ville
		width = random.choice([5,7,9,11])
		height = random.choice([5,7,9,11])

		#on génère une position de la ville en gérant les débordement
		x = random.randrange(3,get_nb_columns(matrix)- 2 - width)
		y = random.randrange(3,get_nb_lines(matrix)- 2 - height)

		#on fait en sorte qu'elles soient impaire pour ne pas avoir de problème lors de la génération du labyrinthe et on évite tout débordement de la map
		while x%2 == 0:
			x = random.randrange(3,get_nb_columns(matrix)- 2 - width)
		while y%2 == 0:
			y = random.randrange(3,get_nb_lines(matrix)- 2 - height)

		#on mets la première ville forcement dans la map, il y a pas de chevauchement dans cette situation
		if(len(list_cities) == 0):
			list_cities.append(City(x, y, width, height))

		#compteur qui permet de regarder si on a comparer avec toute les villes lorsque l'on regarde si il y a un chevauchement ou non
		#rmq : cnt abréviation de counter
		cnt = 0

		#on regarde si elles sont déjà présente dans la liste ou si elle se superpose
		for city in list_cities:
			#on récupère les informations de la ville que l'on cherche à comparer
			x_city = city.get_x_corner()
			y_city = city.get_y_corner()
			width_city = city.get_width()
			height_city = city.get_height()

			#pour que l'on regarde si deux villes ne se supperpose pas,ce compteur permet de comparer les cordonné avec toute les villes
			#on a comparer avec une autre ville
			cnt += 1

			#si la nouvelle ville ne chevauche pas de villes parmit celle déjà étudié
			if(overlap == False):
				#on regarde si la nouvelle ville ne chevauche pas la ville étudiée qui est déjà placée sur l'axe des X puis sur l'axe des Y
				if((x + width + 1 < x_city or x > x_city + width_city + 1) or (y + height + 1 < y_city or y > y_city + height_city +1)):
					#on a regardé que la nouvelle ville ne chevauche aucune villes | cnt permet de dire si on c'est intéréssé a toute les villes ou non, la nouvelle ville pourait très bien ne pas chevaucher la première ville mais chevaucher la seconde par exemple
					if(cnt == len(list_cities)):
						list_cities.append(City(x, y, width, height))
				else:
					#la ville chevauche une autre ville déjà existante, on l'indique alors
					overlap = True

	#on écrit l'emplacement des villes dans la matrice de jeu
	for city in list_cities:
		#on récupère les différentes informations de la ville que l'on va déssiner
		x = city.get_x_corner()
		y = city.get_y_corner()
		width = city.get_width()
		height = city.get_height()

		for i in range(width):
			for j in range(height):
				set_val(matrix,y+j,x+i,CITY)

	return list_cities

def create_maze(matrix):
	"""
	Génère un labyrinthe qui servira a faire les routes par la suite
	"""
	nb_cases_visited = 0 #stock le nombre de case visité
	
	#on intitialise le point de début d'où commence le robot
	current_case = START_CASE.copy() 
	
	#on change la valeur dans la matrice pour lancer un point de lancement de la génération
	set_val(matrix,START_CASE[0],START_CASE[1],PATH)

	#on stocke les culs de sac pour pouvoir retirer les impasses par la suite
	list_end_path = [START_CASE]

	#liste qui va stocker les intersection
	list_intersection = []

	list_positions=[START_CASE]
	
	#status de la génération
	ends = False
	first_turn = True
	back = False

	#tant que la génération n'est pas finit
	while(not ends):
		list_neighbours=[]
		#on regarde dans qu'elle direction le "robot" peut aller
		#on regarde si il n'y a pas de chemin deux bloque plus loin, si c'est le cas, on ajoute la position dans la liste
		#le robot quand il avance se déplace de deux bloques : le premier s'agit du mur séparant deux chemins, et le deuxième la nouvelle intersection ou le robot va de nouveau regardé où il peut aller
		
		#on regade si le robot peut se déplacer à gauche, cette première conditionelle permet de gérer un débordement
		if(current_case[0]-2 >= 0):
			#on regarde si le deuxième bloc à gauche s'agit bien d'un murs et non d'un chemin 
			if(get_val(matrix,current_case[0]-2,current_case[1]) == WALL):
				#alors le robot peut aller à gauche, on l'ajoute dfans la liste des potentielles futures orientations 
				list_neighbours.append((current_case[0]-2,current_case[1]))
		#on fait de même pour les autres orientations
		if(current_case[0]+2 < get_nb_columns(matrix)):
			if(get_val(matrix,current_case[0]+2,current_case[1]) == WALL):
				list_neighbours.append((current_case[0]+2,current_case[1]))
		if(current_case[1]-2 >= 0):
			if(get_val(matrix,current_case[0],current_case[1]-2) == WALL):
				list_neighbours.append((current_case[0],current_case[1]-2))
		if(current_case[1]+2 < get_nb_lines(matrix)):
			if(get_val(matrix,current_case[0],current_case[1]+2) == WALL):
				list_neighbours.append((current_case[0],current_case[1]+2))

		#on regarde si il y a une solution
		if(len(list_neighbours) > 0):
			#on regarde si le robot a finit de faire demie tour, et qu'il décide de partir dans un nouveau coin pas exploré, c'est alors une intersection
			if(back == True):
				back = False
				#on l'ajoute parmit la liste des intersections
				list_intersection.append((current_case[1],current_case[0]))


			#on prend une direction au hasard parmis les cases dans la liste
			neighbour_choose = random.choice(list_neighbours)

			#on fait déplacer réellement le robot, on lui change alors les coordonnées
			new_x = (current_case[0]-neighbour_choose[0])//2
			new_y = (current_case[1]-neighbour_choose[1])//2

			#on casse les deux murs
			current_case[0] += -new_x
			current_case[1] += -new_y
			
			set_val(matrix,current_case[0],current_case[1],PATH)

			list_positions.append([current_case[0],current_case[1]])

			current_case[0] -= new_x
			current_case[1] -= new_y

			set_val(matrix,current_case[0],current_case[1],PATH)

			list_positions.append([current_case[0],current_case[1]])

		else:
			#si le robot ne peut aller dans aucune direction, 
			#on a un cul de sac
			if(back == False):
				list_end_path.append((current_case[1],current_case[0]))
				back = True


			#il doit alors faire demie tour, on récupère alors les anciennent coordonnée
			current_case[0] = list_positions[len(list_positions)-3][0]
			current_case[1] = list_positions[len(list_positions)-3][1]

			list_positions.pop(len(list_positions)-1)
			list_positions.pop(len(list_positions)-1)

		#si le robot est revenu sur la case de départ, alors il a finit de générer le labyrinthe
		if(current_case == [START_CASE[0],START_CASE[1]]):
			ends = True

	return (list_end_path, list_intersection)

def city_access(matrix,list_cities,list_end_path, list_intersection):
	"""
	Relie les villes au Chemin
	"""
	for city in list_cities:

		#on récupère les coordonné de la ville
		x_city = city.get_x_corner()
		y_city = city.get_y_corner()
		width_city = city.get_width()
		height_city = city.get_height()

		#stock les casser que l'on peut casser
		buffer_list=[]

		#on mets dans la liste tout les murs qui longe la ville avec un chemin a coté 
		#on regarde si il peut y avoir un chemins en bas et en haut, on dois donc regarder les débordements
		for i in range(width_city): 
			#on s'intéresse aux bloques en haut de la ville
			#on regarde si la deuxième cases (hors ville) au dessus de chaque case situé en haut de la ville (appartient a la ville)
			#get_val(matrice, ligne, colonne)
			if(y_city-2 >= 0):
				if(get_val(matrix, y_city-2, x_city+i) == PATH):
					#il y a alors possibilité de faire l'accés de la ville par cette endroit
					#on a -1 pour cibler non pas le chemin mais le murs à détruire
					buffer_list.append((x_city+i,y_city-1,"TOP"))			

			#on s'intéresse aux bloques en bas de la ville, on fait de même ...
			if(y_city+height_city+2-1 < get_nb_lines(matrix)):
				if(get_val(matrix, y_city+height_city+2-1, x_city+i) == PATH):
					buffer_list.append((x_city+i,y_city+height_city, "BOT"))

		#on fait de même mais cette fois ci pour les côté gauche et droit de la ville
		for j in range(height_city):
			#gauche
			if(x_city-2 >= 0):
				if(get_val(matrix, y_city+j, x_city-2) == PATH):
					buffer_list.append((x_city-1,y_city+j,"L"))
			#droite
			if(x_city+width_city+2-1 < get_nb_lines(matrix)):
				if(get_val(matrix, y_city+j, x_city+width_city+2-1) == PATH):
					buffer_list.append((x_city+width_city,y_city+j,"R"))

		#on prend une case random étant un mur du côté de la ville
		entrance_city = random.choice(buffer_list)

		while((entrance_city[0]+1, entrance_city[1]+1) in list_intersection or (entrance_city[0]-1, entrance_city[1]-1) in list_intersection or (entrance_city[0]+1, entrance_city[1]-1) in list_intersection or (entrance_city[0]-1, entrance_city[1]+1) in list_intersection):
			entrance_city = random.choice(buffer_list)

		city.set_x_output(entrance_city[0])
		city.set_y_output(entrance_city[1])
		city.set_orientation_output(entrance_city[2])

		#on supprime les éventuelles impasses présente à l'endroit de l'ouverture des villes
		if(entrance_city[2] == "TOP"):
			if((entrance_city[0],entrance_city[1]-1) in list_end_path):
				list_end_path.remove((entrance_city[0],entrance_city[1]-1))
			#le fait de casser un murs pour rejoindre le labyrinthe peut entrainer potentionellement une intersection,
			#il est toute fois posible que ce n'est pas une intersection, on va le check plustard
			else:
				list_intersection.append((entrance_city[0],entrance_city[1]-1))
		if(entrance_city[2] == "BOT"):
			if((entrance_city[0],entrance_city[1]+1) in list_end_path):
				list_end_path.remove((entrance_city[0],entrance_city[1]+1))
			else:
				list_intersection.append((entrance_city[0],entrance_city[1]+1))
		if(entrance_city[2] == "L"):
			if((entrance_city[0]-1,entrance_city[1]) in list_end_path):
				list_end_path.remove((entrance_city[0]-1,entrance_city[1]))
			else:
				list_intersection.append((entrance_city[0]-1,entrance_city[1]))
		if(entrance_city[2] == "R"):
			if((entrance_city[0]+1,entrance_city[1]) in list_end_path):
				list_end_path.remove((entrance_city[0]+1,entrance_city[1]))		
			else:
				list_intersection.append((entrance_city[0]+1,entrance_city[1]))	

		#on ouvre l'accès à la ville
		#entrance_city[1] = ligne
		set_val(matrix, entrance_city[1], entrance_city[0],PATH)

def counter_path_around(matrix, x, y):
	"""
	Compte le nombre de chemin autours d'une case
	"""
	counter_path = 0 
	if(get_val(matrix, y, x-1) != WALL):
		counter_path += 1 
	#si la case chemin d'avant est à droite
	if(get_val(matrix, y, x+1) != WALL):
		counter_path += 1 
		#si la case chemin d'avant est en haut
	if(get_val(matrix, y-1, x) != WALL):
		counter_path += 1
	#si la case chemin d'avant est en bas
	if(get_val(matrix, y+1, x) != WALL):
		counter_path += 1 

	return counter_path

def remove_dead_end(matrix,list_end_path):
	"""
	supprime les impasses, et génère la liste des intersections
	"""
	for end in list_end_path:

		x_path = end[0]
		y_path = end[1]

		counter_path = 1
		x_path_buff = 0
		y_path_buff = 0 

		#tant que l'impasse n'a pas était totalement supprimé
		while(counter_path < 2):
			x_path += x_path_buff
			y_path += y_path_buff

			set_val(matrix, y_path, x_path, WALL)

			x_path_buff = 0
			y_path_buff = 0 

			#si la case chemin d'avant est à gauche
			if(get_val(matrix, y_path, x_path-1) == PATH or get_val(matrix, y_path, x_path-1) == "u"):
				x_path_buff = -1 
			#si la case chemin d'avant est à droite
			if(get_val(matrix, y_path, x_path+1) == PATH or get_val(matrix, y_path, x_path+1) == "u"):
				x_path_buff = 1 
			#si la case chemin d'avant est en haut
			if(get_val(matrix, y_path-1, x_path) == PATH or get_val(matrix, y_path-1, x_path) == "u"):
				y_path_buff = -1 
			#si la case chemin d'avant est en bas
			if(get_val(matrix, y_path+1, x_path) == PATH or get_val(matrix, y_path+1, x_path) == "u"):
				y_path_buff = 1 

			#on regarde si la case suivante n'est pas une intersection
			counter_path = counter_path_around(matrix, x_path+x_path_buff, y_path+y_path_buff)

def correct_list_intersection(matrix, list_intersection):
	"""
	Retire de la liste des intersections les intersections qui n'existe plus (recouvert par un murs, ...)
	"""
	list_buff = list_intersection.copy()

	for intersection in list_intersection:
		if(get_val(matrix, intersection[1], intersection[0]) == PATH):
			#on compte le nombre de chemin autour de l'intersections
			counter_path = counter_path_around(matrix, intersection[0], intersection[1])
			#si il y a moins de 2 chemins, cela signifie que soit c'est au milieu d'un chemin, soit une impasse, soit en plein milieu de chemin
			if(counter_path <= 2):
				#ce n'est donc plus une intersection
				list_buff.remove(intersection)
		else:
			#la case étudié n'est pas un chemin, elle ne peut être une intersection
			list_buff.remove(intersection)

	return list_buff

def draw_rect(matrix, x_top_left_corner, y_top_left_corner, width, height):
	"""
	Dessiner un rectangle de maison
	"""
	width -= 1
	height -= 1

	nb_houses = 0

	#horizontale
	for i in range(1,width):

		if(height == 0):
			nb_houses += 1
			set_val(matrix, y_top_left_corner, x_top_left_corner+i, random.choice(HOUSES))
		else:
			nb_houses += 2
			set_val(matrix, y_top_left_corner, x_top_left_corner+i, random.choice(HOUSES))
			set_val(matrix, y_top_left_corner+height, x_top_left_corner+i, random.choice(HOUSES))

	#verticale
	for j in range(1,height):
		if(width == 0):
			nb_houses += 1
			set_val(matrix, y_top_left_corner+j, x_top_left_corner, random.choice(HOUSES))
		else:
			nb_houses += 2
			set_val(matrix, y_top_left_corner+j, x_top_left_corner, random.choice(HOUSES))
			set_val(matrix, y_top_left_corner+j, x_top_left_corner+width, random.choice(HOUSES))


	return nb_houses

def draw_circle(matrix_reduce, x_center, y_center, radius):
	"""
	Dessine un cercle de maison (Algorithme de tracé d'arc de cercle de Bresenham)
	"""
	nb_houses = 0

	#on commence par en haut du cercle
	x = 0
	y = radius
	m = 5-4*radius
	#on s'arrête quand on a fais un huitième du cercle (1*octant), l'algorithme part du point le plus haut du cercle et descend jusqu'à la bissectrice
	while(x <= y):
		#on utile les propriété de symétrie pour déssiner les autres octants
		if(get_val(matrix_reduce, y+y_center, x+x_center) == CITY):
			set_val(matrix_reduce, y+y_center, x+x_center, random.choice(HOUSES)) 
			nb_houses += 1
		if(get_val(matrix_reduce, x+y_center, y+x_center) == CITY):
			set_val(matrix_reduce, x+y_center, y+x_center, random.choice(HOUSES))
			nb_houses += 1 
		if(get_val(matrix_reduce, y+y_center, -x+x_center) == CITY):
			set_val(matrix_reduce, y+y_center, -x+x_center, random.choice(HOUSES))
			nb_houses += 1
		if(get_val(matrix_reduce, x+y_center, -y+x_center) == CITY): 
			set_val(matrix_reduce, x+y_center, -y+x_center, random.choice(HOUSES))
			nb_houses += 1 
		if(get_val(matrix_reduce, -y+y_center, x+x_center) == CITY):
			set_val(matrix_reduce, -y+y_center, x+x_center, random.choice(HOUSES))
			nb_houses += 1
		if(get_val(matrix_reduce, -x+y_center, y+x_center) == CITY): 
			set_val(matrix_reduce, -x+y_center, y+x_center, random.choice(HOUSES))
			nb_houses += 1
		if(get_val(matrix_reduce, -y+y_center, -x+x_center) == CITY): 
			set_val(matrix_reduce, -y+y_center, -x+x_center, random.choice(HOUSES))
			nb_houses += 1

		if(get_val(matrix_reduce, -x+y_center, -y+x_center) == CITY): 
			set_val(matrix_reduce, -x+y_center, -y+x_center, random.choice(HOUSES))
			nb_houses += 1

		#si M est au dessus du cercle idéale
		if(m > 0):
			y = y-1
			m = m - 8*y

		x=x+1
		m=m+8*x+4 

	return nb_houses

def spread_trees(matrix_reduce, x, y):
	"""
	Retire les bloc de ville danss les coins des villes, qui ne sont pas accessible 
	"""

	set_val(matrix_reduce, y,x, WALL)
	
	if(get_val(matrix_reduce,y,x+1) == CITY):
		spread_trees(matrix_reduce, x+1, y)

	if(get_val(matrix_reduce,y,x-1) == CITY):
		spread_trees(matrix_reduce, x-1, y)

	if(get_val(matrix_reduce,y+1,x) == CITY):
		spread_trees(matrix_reduce, x, y+1)

	if(get_val(matrix_reduce,y-1,x) == CITY):
		spread_trees(matrix_reduce, x, y-1)

def destroy_inaccessible_homes(matrix_reduce, city):
	"""
	Détruit les habitation que l'on ne peut pas y rentrer, 
	"""
	nb_houses_destroy = 0

	#on parcour la ville étudiée
	for i in range(city.get_x_corner(), city.get_x_corner()+city.get_width()+1):
		for j in range(city.get_y_corner(), city.get_y_corner()+city.get_height()+1):
			#on regarde si la case étudiée est une maison
			if(get_val(matrix_reduce, j, i) in HOUSES):
				#on détruit la maison si celle-ci est inaccessible
				if((get_val(matrix_reduce, j, i+1) in HOUSES and get_val(matrix_reduce, j+1, i) in HOUSES)
					or (get_val(matrix_reduce, j, i+1) in HOUSES and get_val(matrix_reduce, j-1, i) in HOUSES)
					or (get_val(matrix_reduce, j, i-1) in HOUSES and get_val(matrix_reduce, j+1, i) in HOUSES)
					or (get_val(matrix_reduce, j, i-1) in HOUSES and get_val(matrix_reduce, j-1, i) in HOUSES)):
					set_val(matrix_reduce, j,i, WALL)
					#on détruit une maison, on doit alors décrémenter le nombre de maison de la ville en question
					nb_houses_destroy += 1

	return (matrix_reduce,nb_houses_destroy)

def generate_houses_1(matrix_reduce,city, orientation):
    """
    Ajouter des habitations dans chaque ville
    Les bâtiments sont situé sur plusieurs lignes horizontale ou verticale, séparées d'un espace qui représente une route
    """
    nb_houses=0

    #on parcours les villes sur l'axe des x
    for i in range(city.get_x_corner(),city.get_x_corner()+city.get_width()):
    	#on parcours les villes sur l'axe des y
        for j in range(city.get_y_corner(),city.get_y_corner()+city.get_height()):
            if(orientation == "HORIZONTAL"):
                if((get_val(matrix_reduce,j-1,i) == CITY and get_val(matrix_reduce,j+1,i) == CITY and i != city.get_x_corner() and i != city.get_x_corner()+city.get_width()-1)):
                    set_val(matrix_reduce,j,i,random.choice(HOUSES))
                    nb_houses += 1
            else:
                if((get_val(matrix_reduce,j,i-1) == CITY and get_val(matrix_reduce,j,i+1) == CITY and j != city.get_y_corner() and j != city.get_y_corner()+city.get_height()-1)):
                    set_val(matrix_reduce,j,i,random.choice(HOUSES))
                    nb_houses += 1

    return nb_houses
	

def generate_houses_2(matrix_reduce,city):
	"""
	Dessine une ville circulaire
	"""
	nb_houses_1 = 0
	nb_houses_2 = 0

	x_center = city.get_x_corner()+city.get_width()//2
	y_center = city.get_y_corner()+city.get_height()//2
	radius = math.trunc(min(city.get_height(),city.get_width())/2) 

	nb_houses_1 = draw_circle(matrix_reduce, x_center, y_center, radius)
	if(min(city.get_width(),city.get_height()) != 9):
		nb_houses_2 = draw_circle(matrix_reduce, x_center, y_center, radius-2)
	else:
		nb_houses_2 = draw_circle(matrix_reduce, x_center, y_center, radius-3)

	if(nb_houses_2 > 1):
		spread_trees(matrix_reduce, x_center, y_center)
	
	matrix_reduce, nb_houses_destroy = destroy_inaccessible_homes(matrix_reduce, city)

	spread_trees(matrix_reduce, city.get_x_corner(), city.get_y_corner())
	spread_trees(matrix_reduce, city.get_x_corner()+city.get_width()-1, city.get_y_corner())
	spread_trees(matrix_reduce, city.get_x_corner(), city.get_y_corner()+city.get_height()-1)
	spread_trees(matrix_reduce, city.get_x_corner()+city.get_width()-1, city.get_y_corner()+city.get_height()-1)

	break_wall_city(matrix_reduce, city)

	#on a cassé une maison en créant l'accés, d'où le -1 dans le return, et nb_houses_destroy représente le nombre de maison inaccessible donc détruite
	return (matrix_reduce,nb_houses_1+nb_houses_2-1-nb_houses_destroy)

def generate_houses_3(matrix_reduce,city):
	"""
	Dessine une ville rectangulaire
	"""
	nb_houses_1 = draw_rect(matrix_reduce, city.get_x_corner(), city.get_y_corner(), city.get_width(), city.get_height())
	nb_houses_2 = 0

	rnd = random.randint(1,3)
	
	if(rnd == 1):
		city_buffer = City(city.get_x_corner()+2, city.get_y_corner()+2, city.get_width()-4, city.get_height()-4)
		nb_houses_2 = draw_rect(matrix_reduce, city_buffer.get_x_corner(), city_buffer.get_y_corner(), city_buffer.get_width(), city_buffer.get_height())

		if( city_buffer.get_width() > 1 and city_buffer.get_height() > 1):
			x_center = city.get_x_corner()+city.get_width()//2
			y_center = city.get_y_corner()+city.get_height()//2
			spread_trees(matrix_reduce, x_center, y_center)
	elif(rnd == 2):
		city_buffer = City(city.get_x_corner()+1, city.get_y_corner()+1, city.get_width()-2, city.get_height()-2)
		nb_houses_2 = generate_houses_1(matrix_reduce,city_buffer, "HORIZONTAL")
	else:
		city_buffer = City(city.get_x_corner()+2, city.get_y_corner()+1, city.get_width()-4, city.get_height()-2)
		nb_houses_2 = generate_houses_1(matrix_reduce,city_buffer, "VERTICAL")

	#on supprime les cases city innacessibles dans les coins de la ville
	spread_trees(matrix_reduce, city.get_x_corner(), city.get_y_corner())
	spread_trees(matrix_reduce, city.get_x_corner()+city.get_width()-1, city.get_y_corner())
	spread_trees(matrix_reduce, city.get_x_corner(), city.get_y_corner()+city.get_height()-1)
	spread_trees(matrix_reduce, city.get_x_corner()+city.get_width()-1, city.get_y_corner()+city.get_height()-1)
	
	break_wall_city(matrix_reduce, city)

	#on a cassé une maison en créant l'accés, d'où le -1 dans le return
	return nb_houses_1+nb_houses_2-1

def break_wall_city(matrix_reduce, city):
	"""
	Fait en sorte de relier les chemins des routes aux cehmins des
	"""
	orientation = city.get_orientation_output()
	x = city.get_x_output()
	y = city.get_y_output()

	running = True
	while(running):

		if(orientation == "L"):
			if(get_val(matrix_reduce, y+1,x) == CITY or get_val(matrix_reduce, y-1,x) == CITY):
				running = False
			x += 1
		elif(orientation == "R"):
			if(get_val(matrix_reduce, y+1,x) == CITY or get_val(matrix_reduce, y-1,x) == CITY):
				running = False
			x -= 1
		elif(orientation == "TOP"):
			if(get_val(matrix_reduce, y,x+1) == CITY or get_val(matrix_reduce, y,x-1) == CITY):
				running = False
			y += 1
		elif(orientation == "BOT"):
			if(get_val(matrix_reduce, y,x+1) == CITY or get_val(matrix_reduce, y,x-1) == CITY):
				running = False
			y -= 1

		if(running):
			set_val(matrix_reduce, y, x, CITY)
		


def generate_houses(matrix_reduce,list_cities):
	"""
	Ajoute des bâtiment dans les villes
	"""
	for city in list_cities:
		nb_houses = 0
		rnd = random.randint(0,100)

		if(city.get_height() == city.get_width()):
			#ville circulaire, on retourne la matrix_reduce car on a supprimé les maisons inaccessibles
			matrix_reduce,nb_houses = generate_houses_2(matrix_reduce,city)
		elif(rnd <= 33):
			nb_houses = generate_houses_1(matrix_reduce,city,"HORIZONTAL")
		elif(rnd <= 66):
			nb_houses = generate_houses_1(matrix_reduce,city,"VERTICAL")
		else:
			nb_houses = generate_houses_3(matrix_reduce,city)

		city.set_nb_houses(nb_houses)

def enlarges(matrix):
	"""
	Agrandit la matrice, une case devient du 8x8 soit 64 cases
	"""
	values=[]
	for i in range(get_nb_lines(matrix)*NB_DETAILS):
		#on débute une nouvelle ligne
		line=[]
		#on parcours toute les colonnes
		for j in range(get_nb_columns(matrix)*NB_DETAILS):
			#on rajoute toute les valeurs des colonnes présente sur la ligne
			line.append(get_val(matrix,int(i//NB_DETAILS),int(j//NB_DETAILS)))
		#on rajoute une ligne dans la matrice
		values.append(line)

	return {"nb_lines":get_nb_lines(matrix)*NB_DETAILS,
			"nb_columns":get_nb_columns(matrix)*NB_DETAILS,
			"values":values}

def starting_city(list_cities):
	"""
	Retourne la ville de départ
	"""
	return list_cities[0]

def generate_list_output_cities(list_cities):
	"""
	Génère une liste contenant l'ensemble des sorties des villes
	"""
	list_cities_output = []
	for city in list_cities:
		list_cities_output.append((city.get_x_output(), city.get_y_output()))
	return list_cities_output


def travel_road(matrix_reduce, list_intersection, list_cities_output, starting_cell, starting_intersection):
	""" 
	Aller jusqu'à la prochaine intersection
	"""
	current_cell = starting_cell
	list_road = [current_cell]

	while (current_cell not in list_intersection and current_cell not in list_cities_output):

		if(get_val(matrix_reduce, current_cell[1]+1, current_cell[0]) == PATH and (current_cell[0],current_cell[1]+1) != list_road[len(list_road)-1] and (current_cell[0],current_cell[1]+1) != starting_intersection):
			list_road.append(current_cell)
			current_cell = (current_cell[0],current_cell[1]+1)
		elif(get_val(matrix_reduce, current_cell[1]-1, current_cell[0]) == PATH and (current_cell[0],current_cell[1]-1) != list_road[len(list_road)-1] and (current_cell[0],current_cell[1]-1) != starting_intersection):
			list_road.append(current_cell)
			current_cell = (current_cell[0],current_cell[1]-1)
		elif(get_val(matrix_reduce, current_cell[1], current_cell[0]+1) == PATH and (current_cell[0]+1,current_cell[1]) != list_road[len(list_road)-1] and (current_cell[0]+1,current_cell[1]) != starting_intersection):
			list_road.append(current_cell)
			current_cell = (current_cell[0]+1,current_cell[1])
		elif(get_val(matrix_reduce, current_cell[1], current_cell[0]-1) == PATH and (current_cell[0]-1,current_cell[1]) != list_road[len(list_road)-1] and (current_cell[0]-1,current_cell[1]) != starting_intersection):
			list_road.append(current_cell)
			current_cell = (current_cell[0]-1,current_cell[1])

	list_road.append(current_cell)	
	return list_road

def get_path_cells(matrix_reduce, starting_intersection, old_cell, list_cities_output, list_intersection):
	"""
	Donne toute les cases où l'on va redémarer a une intersemection, mais pas là où on est venu
	"""
	res = []

	if(get_val(matrix_reduce, starting_intersection[1]+1, starting_intersection[0]) == PATH and (starting_intersection[0],starting_intersection[1]+1) != old_cell and (starting_intersection[0],starting_intersection[1]+1) not in list_cities_output and (starting_intersection[0],starting_intersection[1]+1) not in list_intersection):
		res.append((starting_intersection[0],starting_intersection[1]+1))
	if(get_val(matrix_reduce, starting_intersection[1]-1, starting_intersection[0]) == PATH and (starting_intersection[0],starting_intersection[1]-1) != old_cell and (starting_intersection[0],starting_intersection[1]-1) not in list_cities_output and (starting_intersection[0],starting_intersection[1]-1) not in list_intersection):
		res.append((starting_intersection[0],starting_intersection[1]-1))
	if(get_val(matrix_reduce, starting_intersection[1], starting_intersection[0]+1) == PATH and (starting_intersection[0]+1,starting_intersection[1]) != old_cell and (starting_intersection[0]+1,starting_intersection[1]) not in list_cities_output and (starting_intersection[0]+1,starting_intersection[1]) not in list_intersection):
		res.append((starting_intersection[0]+1,starting_intersection[1]))
	if(get_val(matrix_reduce, starting_intersection[1], starting_intersection[0]-1) == PATH and (starting_intersection[0]-1,starting_intersection[1]) != old_cell and (starting_intersection[0]-1,starting_intersection[1]) not in list_cities_output and (starting_intersection[0]-1,starting_intersection[1]) not in list_intersection):
		res.append((starting_intersection[0]-1,starting_intersection[1]))

	return res

def road_numbering(matrix_reduce,list_intersection, list_cities_output, starting_cell, starting_intersection, parent_road):
	"""
	Numérote les routes
	"""
	road = travel_road(matrix_reduce,list_intersection, list_cities_output, starting_cell,starting_intersection)
	parent_road.append(road[1:len(road)])
	starting_intersection = road[len(road)-1]
	new_starting_cells = get_path_cells(matrix_reduce, starting_intersection,road[len(road)-2], list_cities_output, list_intersection)

	children_roads = []

	for cell in new_starting_cells:
		road_numbering(matrix_reduce,list_intersection, list_cities_output,cell,starting_intersection, parent_road)
		
	return parent_road

def start_road_numbering(matrix_reduce,list_cities,list_intersection,list_cities_output):
	"""
	Appel les fonction nécessaire à la numérotation des routes
	"""	
	starting_cell = (starting_city(list_cities).get_x_output(),starting_city(list_cities).get_y_output())
	starting_intersection = None
	list_road = road_numbering(matrix_reduce, list_intersection, list_cities_output[1:len(list_cities_output)], starting_cell, starting_intersection, [])
	if(len(list_road[0]) > 2):
		layer_road = convert_list_road_into_layer(list_road)
	else:
		layer_road = convert_list_road_into_layer(list_road[1:len(list_road)])

	return layer_road, list_road

def convert_list_road_into_layer(list_road):
	"""
	Transforme la liste des routes en une matrice avec tout les numéro de route sur chaque case
	"""
	layer_road = matrix(NB_LINES,NB_COLUMNS,WALL)
	for index_road in range(len(list_road)):
		for cell_road in list_road[index_road]:
			set_val(layer_road, cell_road[1], cell_road[0], index_road+1)

	return layer_road

def spread_wild_grass(matrix_enlarges, x, y, probability):
	"""
	Répend l'herbe sauvage
	"""
	set_val(matrix_enlarges, y,x, WILD_GRASS)

	right_proba = random.randint(0,100)
	left_proba = random.randint(0,100)
	top_proba = random.randint(0,100)
	bot_proba = random.randint(0,100)

	if( right_proba < probability):
		if(get_val(matrix_enlarges,y,x+1) in [PATH,GRASS]):
			spread_wild_grass(matrix_enlarges, x+1, y, probability-10)
	if( left_proba < probability):
		if(get_val(matrix_enlarges,y,x-1) in [PATH,GRASS]):
			spread_wild_grass(matrix_enlarges, x-1, y, probability-10)
	if( bot_proba < probability):
		if(get_val(matrix_enlarges,y+1,x) in [PATH,GRASS]):
			spread_wild_grass(matrix_enlarges, x, y+1, probability-10)
	if( top_proba < probability):
		if(get_val(matrix_enlarges,y-1,x) in [PATH,GRASS]):
			spread_wild_grass(matrix_enlarges, x, y-1, probability-10)

def starting_wild_grass(matrix_enlarges, road, index_cell_road, offset_wild_grass):
	"""
	Créer les points de départ pour répendre l'herbe sauvage
	sachant qu'il y a 50% de chance de mettre un pts de départ d'herbe sauvage toute les trois cases
	"""
	#pour éviter d'avoir des routes sans herbe, on force à en mettre un pts de départ sur la première case
	if(index_cell_road == 0):
		spread_wild_grass(matrix_enlarges, road[index_cell_road][0]*NB_DETAILS+random.randint(0,NB_DETAILS-1), road[index_cell_road][1]*NB_DETAILS+random.randint(0,NB_DETAILS-1), 100)
	if(index_cell_road%3 == offset_wild_grass):
		#il y a 50% de chance de lancer un point de départ pour l'herbe 
		proba = random.randint(0,100)
		if(proba < 60):
			spread_wild_grass(matrix_enlarges, road[index_cell_road][0]*NB_DETAILS+random.randint(0,NB_DETAILS-1), road[index_cell_road][1]*NB_DETAILS+random.randint(0,NB_DETAILS-1), 100)

def generate_trees_grass(matrix_enlarges, cell_road):
	"""
	Génère les arbres et l'herbe en bordure des routes pour que cela soit un peut plus jolie visuellement
	"""
	#si a gauche c'est une case murs
	if((get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS//2,cell_road[0]*NB_DETAILS-NB_DETAILS//2) == WALL)):
		for i in range(NB_DETAILS):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i,cell_road[0]*NB_DETAILS,random.choice([GRASS,GRASS,WALL]))
			#en diagonale
			if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i,cell_road[0]*NB_DETAILS) == WALL):
				set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i,cell_road[0]*NB_DETAILS+1, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i+1,cell_road[0]*NB_DETAILS+1) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i+1,cell_road[0]*NB_DETAILS+1, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i-1,cell_road[0]*NB_DETAILS+1) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i-1,cell_road[0]*NB_DETAILS+1, GRASS)
		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS-1,cell_road[0]*NB_DETAILS) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS-1,cell_road[0]*NB_DETAILS,GRASS)
		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS,cell_road[0]*NB_DETAILS) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS,cell_road[0]*NB_DETAILS,GRASS)

	#si a droite c'est une case murs
	if((get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS//2,cell_road[0]*NB_DETAILS+NB_DETAILS+NB_DETAILS//2) == WALL)):
		for i in range(NB_DETAILS):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i,cell_road[0]*NB_DETAILS+NB_DETAILS-1,random.choice([GRASS,GRASS,WALL]))

			if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i,cell_road[0]*NB_DETAILS+NB_DETAILS-1) == WALL):
				set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i,cell_road[0]*NB_DETAILS+NB_DETAILS-2, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i+1,cell_road[0]*NB_DETAILS+NB_DETAILS-2) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i+1,cell_road[0]*NB_DETAILS+NB_DETAILS-2, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i-1,cell_road[0]*NB_DETAILS+NB_DETAILS-2) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+i-1,cell_road[0]*NB_DETAILS+NB_DETAILS-2, GRASS)

		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS-1,cell_road[0]*NB_DETAILS+NB_DETAILS-1) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS-1,cell_road[0]*NB_DETAILS+NB_DETAILS-1,GRASS)
		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS,cell_road[0]*NB_DETAILS+NB_DETAILS-1) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS,cell_road[0]*NB_DETAILS+NB_DETAILS-1,GRASS)
	#si en haut c'est une case murs
	if((get_val(matrix_enlarges,cell_road[1]*NB_DETAILS-NB_DETAILS//2,cell_road[0]*NB_DETAILS+NB_DETAILS//2) == WALL)):
		for i in range(NB_DETAILS):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS,cell_road[0]*NB_DETAILS+i,random.choice([GRASS,GRASS,WALL]))
			if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS,cell_road[0]*NB_DETAILS+i) == WALL):
				set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+1,cell_road[0]*NB_DETAILS+i, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+1,cell_road[0]*NB_DETAILS+i-1) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+1,cell_road[0]*NB_DETAILS+i-1, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+1,cell_road[0]*NB_DETAILS+i+1) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+1,cell_road[0]*NB_DETAILS+i+1, GRASS)

		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS,cell_road[0]*NB_DETAILS-1) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS,cell_road[0]*NB_DETAILS-1,GRASS)
		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS,cell_road[0]*NB_DETAILS+NB_DETAILS) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS,cell_road[0]*NB_DETAILS+NB_DETAILS,GRASS)
	#si en bas c'est une case murs
	if((get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS+NB_DETAILS//2,cell_road[0]*NB_DETAILS+NB_DETAILS//2) == WALL)):
		for i in range(NB_DETAILS):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-1,cell_road[0]*NB_DETAILS+i,random.choice([GRASS,GRASS,WALL]))
			if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-1,cell_road[0]*NB_DETAILS+i) == WALL):
				set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-2,cell_road[0]*NB_DETAILS+i, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-2,cell_road[0]*NB_DETAILS+i+1) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-2,cell_road[0]*NB_DETAILS+i+1, GRASS)
				if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-2,cell_road[0]*NB_DETAILS+i-1) == PATH):
					set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-2,cell_road[0]*NB_DETAILS+i-1, GRASS)
		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-1,cell_road[0]*NB_DETAILS-1) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-1,cell_road[0]*NB_DETAILS-1,PATH)
		if(get_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-1,cell_road[0]*NB_DETAILS+NB_DETAILS) == PATH):
			set_val(matrix_enlarges,cell_road[1]*NB_DETAILS+NB_DETAILS-1,cell_road[0]*NB_DETAILS+NB_DETAILS,PATH)


def add_path_borders(matrix_enlarges, cell_road):
	"""
	Ajoute les bordures entre le chemin et l'herbe
	"""
	#co du bloc en haut a gauche de la cellule de la route que l'on cherche a rajouter les bordures
	y = cell_road[1]*NB_DETAILS
	x = cell_road[0]*NB_DETAILS

	list_contact_border = [GRASS,WILD_GRASS]

	for i in range(NB_DETAILS):
		for j in range(NB_DETAILS):
			if(get_val(matrix_enlarges, y+j , x+i) == PATH):
				#on regarde qu'elles sont les blocs autour
				left_cell = get_val(matrix_enlarges, y+j , x+i-1)
				right_cell = get_val(matrix_enlarges, y+j , x+i+1)
				bot_cell = get_val(matrix_enlarges, y+j+1 , x+i)
				top_cell = get_val(matrix_enlarges, y+j-1 , x+i)

				#s'il y a de l'herbe en haut
				if(top_cell in list_contact_border):
					#s'il y a de l'herbe en bas
					if(bot_cell in list_contact_border):
						#s'il y a de l'herbe à gauche
						if(left_cell in list_contact_border):
							#s'il y a de l'herbe à droite
							if(right_cell in list_contact_border):
								#le bloc chemin est entouré par 4 blocs d'herbe, on comble le troup avec de l'herbe, cra normalement ce n'est pas possible
								set_val(matrix_enlarges, y+j , x+i, GRASS)
							else:
								#si toutes les cellules autour sont de l'herbe sauf celle de droite
								set_val(matrix_enlarges, y+j , x+i, BORDER_LTB)
						else:
							 if(right_cell in list_contact_border):
							 	#si toutes les cellules autour sont de l'herbe sauf celle de gauche
							 	set_val(matrix_enlarges, y+j , x+i, BORDER_TRB)
							 else:
							 	#si la cellule de dessus et d'en dessous sont de l'herbe
							 	set_val(matrix_enlarges, y+j , x+i, BORDER_TB)
					#si la case du bas n'est pas de l'herbe
					else: 
						#s'il y a de l'herbe à gauche
						if(left_cell in list_contact_border):
							#s'il y a de l'herbe à droite
							if(right_cell in list_contact_border):
								#si toutes les cellules autour sont de l'herbe sauf celle d'en bas
								set_val(matrix_enlarges, y+j , x+i, BORDER_LTR)
							else:
								#si la cellule de gauche et de dessus sont de l'herbe
								set_val(matrix_enlarges, y+j , x+i, BORDER_LT)
						#s'il n'y a pas d'herbe à gauche
						else:
							#s'il y a de l'herbe à droite
							if(right_cell in list_contact_border):
								#s'il y a de l'herbe à droite, en haut
								set_val(matrix_enlarges, y+j , x+i, BORDER_TR)
							else:
								#s'il y a de l'herbe seulement en haut
								set_val(matrix_enlarges, y+j , x+i, BORDER_T)

				#s'il n'y a pas d'herbe en haut
				else:
					if(bot_cell in list_contact_border):
						if(left_cell in list_contact_border):
							if(right_cell in list_contact_border):
								#si toutes les cellules autour sont de l'herbe sauf celle d'en haut
								set_val(matrix_enlarges, y+j , x+i, BORDER_LRB)
							else:
								#s'il y a de l'herbe en bas et à gauche
								set_val(matrix_enlarges, y+j , x+i, BORDER_BL)
						else:
							if(right_cell in list_contact_border):
								#s'il y a de l'herbe en bas et à droite
								set_val(matrix_enlarges, y+j , x+i, BORDER_BR)
							else:
								#s'il y a de l'herbe seulement en bas
								set_val(matrix_enlarges, y+j , x+i, BORDER_B)
					else:
						if(left_cell in list_contact_border):
							if(right_cell in list_contact_border):
								#s'il y a de l'herbe à gauche et à droite
								set_val(matrix_enlarges, y+j , x+i, BORDER_LR)
							else:
								#s'il y a de l'herbe seulement à gauche
								set_val(matrix_enlarges, y+j , x+i, BORDER_L)
						else:
							if(right_cell in list_contact_border):
								#s'il y a de l'herbe seulement à droite
								set_val(matrix_enlarges, y+j , x+i, BORDER_R)
							#s'il n'y a pas d'herbe alors on ne fait rien

def add_details_road(matrix_enlarges, list_road):
	"""
	Ajout de détails aux cases des routes numéroté
	"""
	for road in list_road:
		for cell_road in road:
			generate_trees_grass(matrix_enlarges, cell_road)
	for road in list_road:
		offset_wild_grass = random.randint(0,2)
		for index_cell_road in range(len(road)):
			starting_wild_grass(matrix_enlarges, road, index_cell_road, offset_wild_grass)
	for road in list_road:
		for cell_road in road:
			add_path_borders(matrix_enlarges, cell_road)

def add_details_road_one_cell(matrix_enlarges, list_road, list_cities_output):
	"""
	Ajout de détails aux cases des routes non numéroté (route de 1 case de long au départ des villes)
	"""
	for city_output in list_cities_output:
		is_numbered = False
		for road in list_road:
			if(city_output == road[-1]):
				is_numbered = True

		if(is_numbered == False):
			generate_trees_grass(matrix_enlarges, city_output)
			add_path_borders(matrix_enlarges, city_output)

def generate_index_center_gym_shop(nb_houses):
    """
    Génère les indices de positionnement des bâtiment spéciaux de la villes qui sont uniques
    """
    index_shop = 0
    index_gym = 0
    index_center = 0

    while (index_shop == index_gym or index_shop == index_center or index_center == index_gym):
        index_shop = random.randint(0,nb_houses-1)
        index_gym = random.randint(0,nb_houses-1)
        index_center = random.randint(0,nb_houses-1)

    return (index_shop, index_gym, index_center)


def add_details_city(matrix_enlarges, matrix_reduce, list_cities):
	"""
	Ajoute des détails aux villes
	"""
	#on parcours chaque ville
	for city in list_cities:

		#on génère l'indice de l'emplacement des bâtiment spéciaux
		index_shop, index_gym, index_center = generate_index_center_gym_shop(city.get_nb_houses())

		#on initialise une variable qui va compter le nombre de maison déjà rencontrer pour placer les batiment spéciaux au bon emplacement
		index_house = 0

		#on parcour la ville étudiée
		for i in range(city.get_x_corner(), city.get_x_corner()+city.get_width()+1):
			for j in range(city.get_y_corner(), city.get_y_corner()+city.get_height()+1):

				#on regarde si la case étudiée est une maison
				if(get_val(matrix_reduce, j, i) in HOUSES):
					
					#si la maison étudiée correspond au centre de soin, on rajoute le centre sur la map dézoomé
					if(index_house == index_center):
						set_val(matrix_reduce,j,i,CENTER)
					#si la maison étudiée correspond à la boutique, on rajoute la boutique sur la map dézoomé
					if(index_house == index_shop):
						set_val(matrix_reduce,j,i,SHOP)
					#si la maison étudiée correspond à l'arène, on rajoute l'arène' sur la map dézoomé
					if(index_house == index_gym):
						set_val(matrix_reduce,j,i,GYM)

					#on cherche par qu'elle valeur commencer la numérotation des différente cases de la maison
					#si on oriente les maisons vers le bas
					if(get_val(matrix_reduce, j+1,i) == CITY):
						cpt = (get_val(matrix_reduce, j,i)+0-CENTER)*(NB_DETAILS*NB_DETAILS)+DATA_RANGE
					#si on oriente les maisons vers le haut 
					elif(get_val(matrix_reduce, j-1,i) == CITY):
						cpt = (get_val(matrix_reduce, j,i)+1-CENTER)*(NB_DETAILS*NB_DETAILS)+DATA_RANGE
					#si il y a de l'air a droite et à gauche, on oriente la maison dans un sens aléatoire (on ne le fait pas pour l'autre orientation, car c'est plus simple à rentrer dans les maisons)
					elif(get_val(matrix_reduce, j,i+1) == CITY and get_val(matrix_reduce, j,i-1) == CITY):
						cpt = random.choice([(get_val(matrix_reduce, j,i)+2-CENTER)*(NB_DETAILS*NB_DETAILS)+DATA_RANGE,(get_val(matrix_reduce, j,i)+3-CENTER)*(NB_DETAILS*NB_DETAILS)+DATA_RANGE])
					#si on oriente les maisons vers la droite
					elif(get_val(matrix_reduce, j,i+1) == CITY):
						cpt = (get_val(matrix_reduce, j,i)+2-CENTER)*(NB_DETAILS*NB_DETAILS)+DATA_RANGE
					#si on oriente les maisons vers la gauche
					elif(get_val(matrix_reduce, j,i-1) == CITY):
						cpt = (get_val(matrix_reduce, j,i)+3-CENTER)*(NB_DETAILS*NB_DETAILS)+DATA_RANGE
						
					#on attribut les chiffres des maisons sur la grande matrice
					for j_house in range(NB_DETAILS):
						for i_house in range(NB_DETAILS):
							set_val(matrix_enlarges, j*NB_DETAILS+j_house, i*NB_DETAILS+i_house, cpt)
							cpt += 1

					#on incrément le compteur qui compte le nombre de maison que l'on a déjà visitée
					index_house += 1

def add_details(matrix_enlarges, matrix_reduce, list_road, list_cities_output, list_cities):
	"""
	Ajout de détails à la map agrandit
	"""
	#si la route ne fais que 1 case de long au départ des villes (elle n'est pas numérotée)
	add_details_road_one_cell(matrix_enlarges, list_road, list_cities_output)
	#on ajoute des détails aux routes qui font plus d'une case (intersections comprises), soit les cases des routes qui sont numérotées
	add_details_road(matrix_enlarges, list_road)
	#on ajoute des détails aux villes
	add_details_city(matrix_enlarges, matrix_reduce, list_cities)
	
def convert_map_zoomed_in_into_zoomed_out(matrix_enlarges):
	"""
	Recrée la carte dézoomée à partir de la carte zoomée
	"""
	values=[]
	for i in range(NB_LINES):
		#on débute une nouvelle ligne
		line=[]
		#on parcours toute les colonnes
		for j in range(NB_COLUMNS):
			#on rajoute toute les valeurs des colonnes présente sur la ligne
			value = get_val(matrix_enlarges, i*NB_DETAILS+NB_DETAILS//2, j*NB_DETAILS+NB_DETAILS//2)

			if(value in [WALL,CITY,PATH]):
				line.append(value)
			elif(value == GRASS or value == WILD_GRASS or value == BORDER_LTR or value == BORDER_TRB or value == BORDER_LRB or value == BORDER_LTB or value == BORDER_LR or value == BORDER_TB or value == BORDER_LT or value == BORDER_TR or value == BORDER_BR or value == BORDER_BL or value == BORDER_T or value == BORDER_B or value == BORDER_L or value == BORDER_R):
				line.append(PATH)
			else:#si c'est une maison
				value = (value-DATA_RANGE)//(NB_DETAILS*NB_DETAILS)+1
				if(value >= CENTER and value < SHOP):
					line.append(CENTER)
				elif(value >= SHOP and value < GYM):
					line.append(SHOP)
				elif(value >= GYM and value < HOUSE1):
					line.append(GYM)
				elif(value >= HOUSE1 and value < HOUSE2):
					line.append(HOUSE1)
				elif(value >= HOUSE2 and value < HOUSE2+4):
					line.append(HOUSE2)
				else:
					print("prob durant la convertion: "+str(value))

		#on rajoute une ligne dans la matrrice
		values.append(line)

	return {"nb_lines":NB_LINES,
			"nb_columns":NB_COLUMNS,
			"values":values}


def generate_map():
	"""
	Génère la map
	"""

	#on crée la plateforme où le joueur va se déplacer
	matrix_reduce = matrix(NB_LINES,NB_COLUMNS,WALL)
	#on rajoute l'emplacement des villes sur la map
	list_cities = place_cities(matrix_reduce)

	#on créer un labyrinthe qui va former les futures routes
	list_end_path, list_intersection = create_maze(matrix_reduce)

	#on créer les accès entre les villes et les routes, pour que le joueur puisse rentrer et sortir des villes
	city_access(matrix_reduce,list_cities, list_end_path, list_intersection)

	#on supprime les culs de sac
	remove_dead_end(matrix_reduce,list_end_path)

	#on vérifie que les interssection de la liste sont encore des intersections malgrès avoir retiré les culs de sac
	list_intersection = correct_list_intersection(matrix_reduce, list_intersection)

	#on génère les maison dans les villes
	generate_houses(matrix_reduce,list_cities)

	#on récupère la liste des sorti de ville
	list_cities_output = generate_list_output_cities(list_cities)

	#on numérote les routes
	layer_road, list_road = start_road_numbering(matrix_reduce,list_cities,list_intersection,list_cities_output)

	#on agrandit la map pour lui donner des détails
	matrix_enlarges = enlarges(matrix_reduce)

	#on ajoute des détails à la map comme des herbes sauvages, ...
	add_details(matrix_enlarges, matrix_reduce, list_road,list_cities_output, list_cities)

	#matrix_reduce = convert_map_zoomed_in_into_zoomed_out(matrix_enlarges)
	
	return (matrix_enlarges, matrix_reduce, list_intersection, layer_road, list_road)

