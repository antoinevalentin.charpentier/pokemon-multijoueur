from model import *
from battle_constants import *
import random
import math

def perctentage_random(percentage):
	"""
	Joue une probabilité d'après un pourcentage*
	"""
	number = random.randint(1,100)
	if number < percentage:
		return True
	return False

def is_alive(team):
	"""
	Retourne un booléeen indiquant si une équipe est en vie
	"""
	for pokemon in team:
		if pokemon.pokemon_remaning_hp > 0:
			return True
	return False

def first_alive_pokemon(team):
	"""
	Retourne le premier pokémon en vie d'une équipe
	Attention l'équipe doit être en vie
	"""
	for pokemon in team:
		if pokemon.pokemon_remaning_hp > 0:
			return pokemon

def get_xp_needeed(team):
	"""
	Retourne l'XP nécéssaire pour passer au level suivant
	"""

	level = team.pokemon_level
	xp_curve = team.pokemon.pokemon_xp_curve

	if xp_curve == "x^3":
		return int(math.pow(level+1,3)) - int(math.pow(level,3))
	elif xp_curve == "\\frac{6x^3}{5} - 15x^2 + 100x - 140":
		return int(6*math.pow(level+1,3)/5 - 15*math.pow(level+1,2) + 100*level+1 - 140) - int(6*math.pow(level,3)/5 - 15*math.pow(level,2) + 100*level - 140)
	elif xp_curve == "\\frac{5x^3}{4}":
		return int(5*math.pow(level+1,3)/4) - int(5*math.pow(level,3)/4)
	else:
		return int(4*math.pow(level+1,3)/5) - int(4*math.pow(level,3)/5)

def level_up(user_pokemon, opponent_pokemon):
	"""
	Monte de niveau un pokémon
	"""
	user_pokemon.pokemon_current_xp += int(opponent_pokemon.pokemon.pokemon_base_experience * (opponent_pokemon.pokemon_level / 7))
	print(get_xp_needeed(user_pokemon))
	while(get_xp_needeed(user_pokemon) < user_pokemon.pokemon_current_xp):
		user_pokemon.pokemon_current_xp -= get_xp_needeed(user_pokemon)
		if(user_pokemon.pokemon_level < 100):
			user_pokemon.pokemon_level += 1
			print(user_pokemon.pokemon_surname + " monte au niveau "+str((user_pokemon.pokemon_level)))

def generate_team_pokemon(session,pokemon_to_generate):
	"""
	Méthode permettant de générer un pokémon (sauvage ou dresseur)
	Prend un pokémon (type Pokemon) en entrée
	Retourne un objet de type Team 
	"""
	evs = [0]*6
	ivs = []
	for i in range(6):
		ivs.append(random.randint(0,31))
	shiny = False
	shiny_number = random.randint(0,2074)
	if shiny_number == 0:
		shiny = True
	print(pokemon_to_generate.pokemon_name)
	abilities = get_abilities(session,pokemon_to_generate)
	ability = random.choice(abilities)
	team = TeamNoDatabaseLink(team_id=get_new_team_id(session),pokemon_evs=str(evs)[1:len(str(evs))-1],pokemon_ivs=str(ivs)[1:len(str(ivs))-1],pokemon_shiny=shiny,
				pokemon_surname=pokemon_to_generate.pokemon_name,pokemon_held_item=None,pokemon_nature=get_random_nature(),
				pokemon_level=50,pokemon_current_xp=0,pokemon_ability=ability,pokemon=pokemon_to_generate.pokemon_to_pokemon_no_database_link())
	team.pokemon_remaning_hp = calculate_max_hp(team)
	pick_four_random_moves(team,pokemon_to_generate)
	return team

def calculate_capture(pokemon,ball):
	"""
	Retourne un booléeen indiquant si la ball a réussie ou échouée
	"""
	random_value = 0
	if ball == "Poké Ball":
		random_value = random.randint(1,4)
	elif ball == "Super Ball":
		random_value = random.randint(1,3)
	elif ball == "Hyper Ball" :
		random_value = random.randint(1,2)
	elif ball == "Master Ball" :
		random_value = 1
	if random_value == 1:
		return True
	return False

def try_capture_pokemon(pokemon,ball):
	"""
	Méthode permettant de capturer ou non un pokémon sauvage
	Paramètres :
	le pokemon et l'aventure, ainsi que la ball lancée
	"""
	print(ball)
	is_captured = calculate_capture(pokemon,ball)
	if is_captured:
		return pokemon
		print("Capture réussie")
	else:
		print("La capture a raté")
		return None

def add_pokemon_to_team(session,pokemon,adventure):
	"""
	Méthode permettant d'ajouter un pokémon à l'équipe d'un trainer
	Paramètres : un pokemon de type Team
				 une aventure de type Adventure
	"""
	pokemon.adventure = adventure
	session.add(pokemon)
	session.commit()

def calculate_max_hp(pokemon_generated):
	"""
	Retourne les pv max pour un pokémon donné
	en fonction de l'espèce et des ivs/evs
	"""
	evs = pokemon_generated.pokemon_evs.split(", ")
	for i in range(len(evs)):
		evs[i] = int(evs[i])
	ivs = pokemon_generated.pokemon_ivs.split(", ")
	for i in range(len(ivs)):
		ivs[i] = int(ivs[i])
	return int(( ( (2*pokemon_generated.pokemon.pokemon_base_pv + ivs[0] + evs[0]/4) * pokemon_generated.pokemon_level ) / 100) + pokemon_generated.pokemon_level + 10)

def heal_team(team):
	"""
	Heal tous les pokémons d'un équipe
	team est une liste d'objets de type TeamNoDatabaseLink
	"""
	for team_pokemon in team:
		team_pokemon.pokemon_remaning_hp = calculate_max_hp(team_pokemon)
		team_pokemon.pokemon_status = None
		team_pokemon.pokemon_first_move_pp = team_pokemon.pokemon_first_move.move_pp
		team_pokemon.pokemon_second_move_pp = team_pokemon.pokemon_second_move.move_pp
		team_pokemon.pokemon_third_move_pp = team_pokemon.pokemon_third_move.move_pp
		team_pokemon.pokemon_fourth_move_pp = team_pokemon.pokemon_fourth_move.move_pp

def initialise_stats(pokemon_generated):
	"""
	Retourne les statistiques d'un pokémon au début du combat
	avant l'activation d'éventuels talents ou objets
	"""
	stats = []
	evs = pokemon_generated.pokemon_evs.split(", ")
	for i in range(len(evs)):
		evs[i] = int(evs[i])
	ivs = pokemon_generated.pokemon_ivs.split(", ")
	for i in range(len(ivs)):
		ivs[i] = int(ivs[i])
	stats.append(int(((2*pokemon_generated.pokemon.pokemon_base_attack + ivs[1] + evs[1]/4) * pokemon_generated.pokemon_level ) / 100 + 5))
	stats.append(int(((2*pokemon_generated.pokemon.pokemon_base_defense + ivs[2] + evs[2]/4) * pokemon_generated.pokemon_level ) / 100 + 5))
	stats.append(int(((2*pokemon_generated.pokemon.pokemon_base_special_attack + ivs[3] + evs[3]/4) * pokemon_generated.pokemon_level ) / 100 + 5))
	stats.append(int(((2*pokemon_generated.pokemon.pokemon_base_special_defense + ivs[4] + evs[4]/4) * pokemon_generated.pokemon_level ) / 100 + 5))
	stats.append(int(((2*pokemon_generated.pokemon.pokemon_base_speed + ivs[5] + evs[5]/4) * pokemon_generated.pokemon_level ) / 100 + 5))
	apply_nature(pokemon_generated,stats)
	return stats

def get_stab(move,attacker):
	"""
	Modifier de stab
	"""
	if attacker.pokemon.pokemon_type1 == move.move_type:
		return 1.5
	if attacker.pokemon.pokemon_type2 == move.move_type:
		return 1.5
	return 1

def get_critical(move):
	"""
	Modifier de crit
	"""
	if random.randint(0,99) < 16 * (1 + move.move_crit_chance):
		print("Coup critique")
		return 1.5
	return 1

def get_burned(attacker):
	"""
	Modifier de burn
	"""
	if attacker.pokemon_status == "burn":
		return 0.5
	return 1

def calculate_modifiers(move,attacker,defender):
	"""
	Calcule le coefficient par lequel il faut multiplier les dommages
	d'une attaque en fonction du STAB, type, météo, ect...
	"""
	return get_coefficient_move_to_pokemon(move,defender) * get_stab(move,attacker) * get_critical(move) * random.randint(85,100)/100 * get_burned(attacker)

def calculate_damage(move,attacker,defender,attacker_stats,defender_stats):
	"""
	Retourne le nombre de dégats d'un move d'un pokémon offensif
	sur un pokémon défensif
	"""
	if move.move_damage_class == "physical":
		return int((((2*attacker.pokemon_level) / 5 + 2) * move.move_power * (attacker_stats[0]/defender_stats[1]) / 50 + 2) * calculate_modifiers(move,attacker,defender))
	elif move.move_damage_class == "special":
		return int((((2*attacker.pokemon_level) / 5 + 2) * move.move_power * (attacker_stats[2]/defender_stats[3]) / 50 + 2) * calculate_modifiers(move,attacker,defender))

def is_move_success(move,attacker_modifiers,defender_modifiers,pokemon):
	"""
	Retourne un booléen indiquant si un move à réussi
	Prend en compte la précision et l'esquive
	"""
	if pokemon.pokemon_status == "paralysis":
		if random.randint(0,3) == 3:
			print(pokemon.pokemon_surname + " est paralysé")
			return False
	if pokemon.pokemon_status == "sleep":
		print(pokemon.pokemon_surname + " dort")
		return False
	if pokemon.pokemon_status == "freeze":
		print(pokemon.pokemon_surname + " est gelé")
		return False
	if pokemon.pokemon_status == "confusion":
		if random.randint(0,1) == 1:
			print(pokemon.pokemon_surname + " se blesse dans sa confusion")
			pokemon.pokemon_remaning_hp = max(0, int(pokemon.pokemon_remaning_hp * (1 - 1/8 )))
			return False
	if move.move_accuracy == None:
		return True
	if perctentage_random(move.move_accuracy * (1 + (0.5 * attacker_modifiers[5])) * (1 - (0.5 * attacker_modifiers[6]))):
		return True
	else:
		print("L'attaque a raté !")
		return False

def apply_move_stats_changes(move,stats_modifiers):
	"""
	Modifie les valeurs des modificateurs de statistiques
	"""
	stats_changed = move.move_stat_changed.split(",")
	stats_amplifiers = move.move_stat_amplifier.split(",")
	for i in range(len(stats_changed)):
		if stats_changed[i] == "attack":
			stats_modifiers[0] = max(-6,min(stats_modifiers[0]+int(stats_amplifiers[i]),6))
		elif stats_changed[i] == "defense":
			stats_modifiers[1] = max(-6,min(stats_modifiers[1]+int(stats_amplifiers[i]),6))
		elif stats_changed[i] == "special-attack":
			stats_modifiers[2] = max(-6,min(stats_modifiers[2]+int(stats_amplifiers[i]),6))
		elif stats_changed[i] == "special-defense":
			stats_modifiers[3] = max(-6,min(stats_modifiers[3]+int(stats_amplifiers[i]),6))
		elif stats_changed[i] == "speed":
			stats_modifiers[4] = max(-6,min(stats_modifiers[4]+int(stats_amplifiers[i]),6))
		elif stats_changed[i] == "accuracy":
			stats_modifiers[5] = max(-6,min(stats_modifiers[5]+int(stats_amplifiers[i]),6))
		elif stats_changed[i] == "evasion":
			stats_modifiers[6] = max(-6,min(stats_modifiers[6]+int(stats_amplifiers[i]),6))

def apply_status_effects_end_turn(user_pokemon, opponent_pokemon):
	"""
	Applique les effets des status sur les pokémons
	"""
	if user_pokemon.pokemon_status == "burn":
		user_pokemon.pokemon_remaning_hp = max(0, int(user_pokemon.pokemon_remaning_hp - calculate_max_hp(user_pokemon) * 1/16))
	elif user_pokemon.pokemon_status == "poison":
		user_pokemon.pokemon_remaning_hp = max(0, int(user_pokemon.pokemon_remaning_hp - calculate_max_hp(user_pokemon) * 1/8))
	elif user_pokemon.pokemon_status == "trap":
		user_pokemon.pokemon_remaning_hp = max(0, int(user_pokemon.pokemon_remaning_hp - calculate_max_hp(user_pokemon) * 1/8))
	elif user_pokemon.pokemon_status == "ingrain":
		user_pokemon.pokemon_remaning_hp = min(calculate_max_hp(user_pokemon), int(user_pokemon.pokemon_remaning_hp + calculate_max_hp(user_pokemon) * 1/16))
	elif user_pokemon.pokemon_status == "leech-seed":
		hp_lost = min(int(calculate_max_hp(user_pokemon) * 1/8,user_pokemon.pokemon_remaning_hp))
		user_pokemon.pokemon_remaning_hp -= hp_lost
		opponent_pokemon.pokemon_remaning_hp = min(calculate_max_hp(opponent_pokemon), opponent_pokemon.pokemon_remaning_hp + hp_lost)

	if opponent_pokemon.pokemon_status == "burn":
		opponent_pokemon.pokemon_remaning_hp = max(0, int(opponent_pokemon.pokemon_remaning_hp - calculate_max_hp(opponent_pokemon) * 1/16))
	elif opponent_pokemon.pokemon_status == "poison":
		opponent_pokemon.pokemon_remaning_hp = max(0, int(opponent_pokemon.pokemon_remaning_hp - calculate_max_hp(opponent_pokemon) * 1/8))
	elif opponent_pokemon.pokemon_status == "trap":
		opponent_pokemon.pokemon_remaning_hp = max(0, int(opponent_pokemon.pokemon_remaning_hp - calculate_max_hp(opponent_pokemon) * 1/8))
	elif opponent_pokemon.pokemon_status == "ingrain":
		opponent_pokemon.pokemon_remaning_hp = min(calculate_max_hp(opponent_pokemon), int(opponent_pokemon.pokemon_remaning_hp + calculate_max_hp(opponent_pokemon) * 1/16))
	elif opponent_pokemon.pokemon_status == "leech-seed":
		hp_lost = min(int(calculate_max_hp(opponent_pokemon) * 1/8),opponent_pokemon.pokemon_remaning_hp)
		opponent_pokemon.pokemon_remaning_hp -= hp_lost
		user_pokemon.pokemon_remaning_hp = min(calculate_max_hp(user_pokemon), user_pokemon.pokemon_remaning_hp + hp_lost)


def apply_status_effects_before_move(pokemon):
	"""
	Applique les effets des status sur les pokémons
	"""
	if pokemon.pokemon_status == "freeze":
		if random.randint(1,5) == 5:
			print(pokemon.pokemon_surname + " s'est dégelé")
			pokemon.pokemon_status = None

	if pokemon.pokemon_status == "sleep":
		if random.randint(1,2) == 2:
			print(pokemon.pokemon_surname + " s'est révéillé")
			pokemon.pokemon_status = None

	if pokemon.pokemon_status == "confusion":
		if random.randint(1,2) == 2:
			print(pokemon.pokemon_surname + " sort de sa confusion")
			pokemon.pokemon_status = None

def get_stats_changed(stats, modifiers, pokemon):
	"""
	Retourne les statistiques modifiées par les boosts et baisses
	"""
	stats_modified = []
	for i in range(len(stats)):
		if modifiers[i] >= 0:
			stats_modified.append(int(stats[i] * ( (2+ modifiers[i])/2 )))
		else:
			stats_modified.append(int(stats[i] * ( 2/(2 - modifiers[i]))))
	if pokemon.pokemon_status == "paralysis":
		stats_modified[4] = stats_modified[4]//2
	if pokemon.pokemon_status == "burn":
		stats_modified[0] = stats_modified[0]//2
	return stats_modified

def attacker_attack_defender(move,attacker,defender,attacker_stats,defender_stats,attacker_modifiers,defender_modifiers):
	"""
	Cette fonction rend compte de l'attaque d'un pokémon sur un autre
	elle applique entre autres les dégats,
	les baisses de stats, les status et autres effets secondaires
	"""

	#Check si le move a réussi
	if attacker.pokemon_remaning_hp > 0:

		print(attacker.pokemon_surname + " utilise " + move.move_name)

		if is_move_success(move,attacker_modifiers,defender_modifiers,attacker):

			#Baisser les pp
			if move.move_id == attacker.pokemon_first_move.move_id:
				attacker.pokemon_first_move_pp -= 1
			elif move.move_id == attacker.pokemon_second_move.move_id:
				attacker.pokemon_second_move_pp -= 1
			elif move.move_id == attacker.pokemon_third_move.move_id:
				attacker.pokemon_third_move_pp -= 1
			elif move.move_id == attacker.pokemon_fourth_move.move_id:
				attacker.pokemon_fourth_move_pp -= 1

			#Récupérer les statistiques avec les boosts et drops
			attacker_stats_after_modifiers = get_stats_changed(attacker_stats, attacker_modifiers, attacker)
			defender_stats_after_modifiers = get_stats_changed(defender_stats, defender_modifiers, defender)

			#PV perdus
			if move.move_power != None:
				if move.move_max_hits != None:
					for n in range(random.randint(move.move_min_hits, move.move_max_hits)):
						hp_lost = calculate_damage(move,attacker,defender,attacker_stats_after_modifiers,defender_stats_after_modifiers)
						defender.pokemon_remaning_hp = max(0,defender.pokemon_remaning_hp - hp_lost)
				else:
					hp_lost = calculate_damage(move,attacker,defender,attacker_stats_after_modifiers,defender_stats_after_modifiers)
					defender.pokemon_remaning_hp = max(0,defender.pokemon_remaning_hp - hp_lost)

			#Baisse(s) de stats
			if move.move_stat_changed != None:
				if move.move_damage_class == "status":
					if move.move_target == "selected-pokemon":
						if defender.pokemon_remaning_hp > 0:
							apply_move_stats_changes(move,defender_modifiers)
					elif move.move_target == "user":
						if attacker.pokemon_remaning_hp > 0:
							apply_move_stats_changes(move,attacker_modifiers)
				else:
					if perctentage_random(move.move_stat_modify_chance):
						if move.move_target == "selected-pokemon":
							if defender.pokemon_remaning_hp > 0:
								apply_move_stats_changes(move,defender_modifiers)
						elif move.move_target == "user":
							if attacker.pokemon_remaning_hp > 0:
								apply_move_stats_changes(move,attacker_modifiers)					

			#Status
			if move.move_status != "none":
				if move.move_damage_class == "status":
					if move.move_target == "selected-pokemon":
						if defender.pokemon_status == None and defender.pokemon_remaning_hp > 0:
							defender.pokemon_status = move.move_status
							print(defender.pokemon_surname + " est "+move.move_status)
					elif move.move_target == "user":
						if attacker.pokemon_status == None and attacker.pokemon_remaning_hp > 0:
							attacker.pokemon_status = move.move_status
							print(attacker.pokemon_surname + " est "+move.move_status)
				else:
					if perctentage_random(move.move_status_chance):
						if defender.pokemon_remaning_hp > 0:
							defender.pokemon_status = move.move_status
							print(defender.pokemon_surname + " est "+move.move_status)

			#PV perdus (recoil) ou gagnés (drain)
			if move.move_power != None:
				if move.move_drain > 0:
					attacker.pokemon_remaning_hp = min(int(attacker.pokemon_remaning_hp + hp_lost * (1 + move.move_drain/100)),calculate_max_hp(attacker))
				elif move.move_drain < 0:
					attacker.pokemon_remaning_hp = max(int(attacker.pokemon_remaning_hp - hp_lost * (1 + move.move_drain/100)),0)

			#Healing
			if move.move_heal > 0:
				attacker.pokemon_remaning_hp = min( (int(attacker.pokemon_remaning_hp * (1 + (move.move_heal / 100)))), calculate_max_hp(attacker))


			#TODO : Status (move de contact et talent éventuel(s))

def choose_swich_pokemon(team,pokemon_in_battle):
	"""
	Retourne un pokémon dans l'équipe (en vie), celui qui va être envoyé au combat lors d'un swich
	"""
	run = True
	while(run):
		print(team)
		team_number = int(input("Choissiez votre pokémon :"))
		if team_number < len(team):
			choosen_pokemon = team[team_number]
			if choosen_pokemon.pokemon_remaning_hp > 0 and choosen_pokemon != pokemon_in_battle:
				run = False
	return choosen_pokemon

def user_move_pick(user_pokemon):
	"""
	Cette fonction retourne le move que l'utilisateur choisi lors d'un combat pokémon
	"""
	moves = [user_pokemon.pokemon_first_move,user_pokemon.pokemon_second_move,user_pokemon.pokemon_third_move,user_pokemon.pokemon_fourth_move]
	pps = [user_pokemon.pokemon_first_move_pp,user_pokemon.pokemon_second_move_pp,user_pokemon.pokemon_third_move_pp,user_pokemon.pokemon_fourth_move_pp]
	run = True
	while run:
		for n in range(len(moves)):
			print( moves[n].move_name + " " + str(pps[n]) + "/" +str(moves[n].move_pp) +"pp")
		number = int(input("Choissiez le move"))
		if number < 4 and pps[number] > 0:
			run = False
	return moves[number]

def opponent_move_pick(opponent_pokemon):
	"""
	Retourne le move choisi par une IA lors d'un combat
	"""
	moves = [opponent_pokemon.pokemon_first_move,opponent_pokemon.pokemon_second_move,opponent_pokemon.pokemon_third_move,opponent_pokemon.pokemon_fourth_move]
	return random.choice(moves)

def choose_battle_item(items):
	"""
	Retourne l'item choisi lors d'un tour d'un combat
	"""
	run = True
	while run:
		for item in items:
			print(item)
		number = int(input("Choissiez l'item a utiliser"))
		if number < len(items):
			run = False
	return items[number]

def use_battle_item(item_used, user_pokemon, opponent_pokemon, adventure, wild):
	"""
	Utilise un item en combat
	"""

	#Items de soin
	if item_used[0] == "Potion":
		user_pokemon.pokemon_remaning_hp = min(user_pokemon.pokemon_remaning_hp + 20,calculate_max_hp(user_pokemon))
		print(user_pokemon.pokemon_surname + " regagne 20 PV")
	elif item_used[0] == "Super Potion":
		user_pokemon.pokemon_remaning_hp = min(user_pokemon.pokemon_remaning_hp + 50,calculate_max_hp(user_pokemon))
		print(user_pokemon.pokemon_surname + " regagne 50 PV")
	elif item_used[0] == "Hyper Potion":
		user_pokemon.pokemon_remaning_hp = min(user_pokemon.pokemon_remaning_hp + 120,calculate_max_hp(user_pokemon))
		print(user_pokemon.pokemon_surname + " regagne 120 PV")
	elif item_used[0] == "Antidote":
		if user_pokemon.pokemon_status == "poison":
			user_pokemon.pokemon_status = None
			print(user_pokemon.pokemon_surname + " est guéri du poison")
	elif item_used[0] == "Réveil":
		if user_pokemon.pokemon_status == "sleep":
			user_pokemon.pokemon_status = None
			print(user_pokemon.pokemon_surname + " se réveille")
	elif item_used[0] == "Anti-Para":
		if user_pokemon.pokemon_status == "paralysis":
			user_pokemon.pokemon_status = None
			print(user_pokemon.pokemon_surname + " est guéri de la paralysie")
	elif item_used[0] == "Antigel":
		if user_pokemon.pokemon_status == "freeze":
			user_pokemon.pokemon_status = None
			print(user_pokemon.pokemon_surname + " se dégèle")
	elif item_used[0] == "Anti-Brulure":
		if user_pokemon.pokemon_status == "burn":
			user_pokemon.pokemon_status = None
			print(user_pokemon.pokemon_surname + " est guéri de la brulûre")
	elif item_used[0] == "Total Soin":
		user_pokemon.pokemon_status = None
		print(user_pokemon.pokemon_surname + " est guéri de tous ses problèmes de statuts")
	elif item_used[0] == "Potion Max":
		user_pokemon.pokemon_remaning_hp = calculate_max_hp(user_pokemon)
		print(user_pokemon.pokemon_surname + " regagne toute sa vie")
	elif item_used[0] == "Guérison":
		user_pokemon.pokemon_remaning_hp = calculate_max_hp(user_pokemon)
		user_pokemon.pokemon_status = None
		print(user_pokemon.pokemon_surname + " regagne toute sa vie et est guéri de tous ses problèmes de statuts")

	#Balls
	elif item_used[0][len(item_used[0])-4:len(item_used[0])] == "Ball":
		if wild:
			item_used[1] = max(0,item_used[1]-1)
			return try_capture_pokemon(opponent_pokemon,item_used[0])
		else:
			print("Vous ne pouvez pas capturer le pokémon d'un dresseur")

	#Enlever l'item utilisé
	item_used[1] = max(0,item_used[1]-1)

def pick_four_random_moves(pokemon_generated, pokemon_to_generate):
	"""
	Instancie les 4 moves d'un pokémon
	"""
	moves = pokemon_to_generate.learn.copy()

	learn = random.choice(moves)
	pokemon_generated.pokemon_first_move = learn.move.move_to_move_no_database_link()
	pokemon_generated.pokemon_first_move_pp = learn.move.move_pp
	moves.remove(learn)

	learn = random.choice(moves)
	pokemon_generated.pokemon_second_move = learn.move.move_to_move_no_database_link()
	pokemon_generated.pokemon_second_move_pp = learn.move.move_pp
	moves.remove(learn)

	learn = random.choice(moves)	
	pokemon_generated.pokemon_third_move = learn.move.move_to_move_no_database_link()
	pokemon_generated.pokemon_third_move_pp = learn.move.move_pp
	moves.remove(learn)

	learn = random.choice(moves)	
	pokemon_generated.pokemon_fourth_move = learn.move.move_to_move_no_database_link()
	pokemon_generated.pokemon_fourth_move_pp = learn.move.move_pp
	moves.remove(learn)