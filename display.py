import pygame
import math
from generate_map import *
from map_constants import *

#on initialise pygame
pygame.init()

#on génère la fenêtre de jeu
pygame.display.set_caption("Génération de la map")
screen = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))

#on charges deux fois les images sous des tailles différentes car cela permeterrais de consommer moins de cpu mais consomme plus de ram

#images zoomed in
#on calcule les dimension d'une case de texture, tel que l'on affiche 1 bloc de murs de chaqeu coté de l'écran quand on marche d'où le -2
#et on veut qu'il y ai 6 cases de chaques côtés du joueur qui est au milieu de l'écran
CASE_WIDTH_IN = math.ceil(SCREEN_WIDTH/(2*(NB_DETAILS-2)+1))
CASE_HEIGHT_IN = math.ceil(SCREEN_HEIGHT/(2*(NB_DETAILS-2)+1))

image_wall_in = pygame.transform.scale(pygame.image.load("assets/map/path/WALL.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_path_in = pygame.transform.scale(pygame.image.load("assets/map/path/PATH.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_city_in = pygame.transform.scale(pygame.image.load("assets/map/city/CITY.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_shop_in = pygame.transform.scale(pygame.image.load("assets/map/city/SHOP.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_gym_in = pygame.transform.scale(pygame.image.load("assets/map/city/GYM.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_center_in = pygame.transform.scale(pygame.image.load("assets/map/city/CENTER.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_house1_in = pygame.transform.scale(pygame.image.load("assets/map/city/HOUSE1.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_house2_in = pygame.transform.scale(pygame.image.load("assets/map/city/HOUSE2.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_wild_grass_in = pygame.transform.scale(pygame.image.load("assets/map/path/WILD_GRASS.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_grass_in = pygame.transform.scale(pygame.image.load("assets/map/path/GRASS.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_player_in = pygame.transform.scale(pygame.image.load("assets/map/PLAYER.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))

#on charge les textures des maisons
images_houses = []
ctr = 0 
for name_house in ["CENTER","GYM","SHOP","HOUSE1","HOUSE2"]:
	for orientation in ["BOT","TOP","RIGHT","LEFT"]:
		ctr += 1
		buff = []
		for i in range(1,64+1):
			buff.append(pygame.transform.scale(pygame.image.load("assets/map/houses/"+str(ctr)+"/images/"+name_house+"_"+orientation+"_"+str(i)+".png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN)))
		images_houses.append(buff)


image_border_b_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_B.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_bl_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_BL.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_br_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_BR.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_l_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_L.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_lr_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_LR.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_lrb_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_LRB.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_lt_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_LT.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_ltb_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_LTB.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_ltr_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_LTR.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_r_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_R.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_t_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_T.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_tb_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_TB.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_tr_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_TR.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))
image_border_trb_in = pygame.transform.scale(pygame.image.load("assets/map/path/BORDER_TRB.png").convert(),(CASE_WIDTH_IN,CASE_HEIGHT_IN))


#images zoomed out
CASE_WIDTH_OUT = int(SCREEN_WIDTH/(NB_COLUMNS))
CASE_HEIGHT_OUT = int(SCREEN_HEIGHT/(NB_LINES))
x_offset = (SCREEN_WIDTH - NB_COLUMNS*CASE_WIDTH_OUT)//2
y_offset = (SCREEN_HEIGHT - NB_LINES*CASE_HEIGHT_OUT)//2

image_red_cross_out = pygame.image.load("assets/button/RED_CROSS.png").convert_alpha()
image_red_cross_out = pygame.transform.scale(image_red_cross_out, (int((58/1280)*SCREEN_WIDTH), int((58/1280)*SCREEN_HEIGHT)))
image_red_cross_out_rect = image_red_cross_out.get_rect()
image_red_cross_out_rect.x = math.ceil(SCREEN_WIDTH - 2*int((58/1280)*SCREEN_WIDTH))
image_red_cross_out_rect.y = math.ceil(int((58/1280)*SCREEN_HEIGHT))

image_intersection_out = pygame.transform.scale(pygame.image.load("assets/map/INTERSECTION.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_wall_out = pygame.transform.scale(pygame.image.load("assets/map/path/WALL.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_path_out = pygame.transform.scale(pygame.image.load("assets/map/path/PATH.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_city_out = pygame.transform.scale(pygame.image.load("assets/map/city/CITY.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_shop_out = pygame.transform.scale(pygame.image.load("assets/map/city/SHOP.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_gym_out = pygame.transform.scale(pygame.image.load("assets/map/city/GYM.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_center_out = pygame.transform.scale(pygame.image.load("assets/map/city/CENTER.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_house1_out = pygame.transform.scale(pygame.image.load("assets/map/city/HOUSE1.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_house2_out = pygame.transform.scale(pygame.image.load("assets/map/city/HOUSE2.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
image_player_out = pygame.transform.scale(pygame.image.load("assets/map/PLAYER.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))

r1 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/1.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r2 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/2.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r3 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/3.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r4 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/4.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r5 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/5.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r6 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/6.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r7 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/7.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r8 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/8.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r9 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/9.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r10 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/10.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r11 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/11.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r12 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/12.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r13 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/13.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r14 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/14.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r15 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/15.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r16 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/16.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r17 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/17.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r18 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/18.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r19 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/19.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r20 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/20.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r21 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/21.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r22 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/22.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r23 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/23.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r24 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/24.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r25 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/25.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r26 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/26.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))
r27 = pygame.transform.scale(pygame.image.load("assets/map/road_numbering/27.png").convert(),(CASE_WIDTH_OUT,CASE_HEIGHT_OUT))



def display_map(aventure, map_display_type, player_coordinates):
	"""
	en attendant aventure = [x,y,matrix_enlarges, matrix_reduce, list_intersection]
	Affiche le jeu au joueur
	map_display_type :
		# "ZOOMED_IN" = en train de se balader dans la map =  
		# "ZOOMED_OUT" = affichage de la map global
		# "START_MENU" = dans le menu
	"""
	
	if(map_display_type == "ZOOMED_IN"):
		
		for i in range(aventure[0]-6,aventure[0]+7):
			for j in range(aventure[1]-6,aventure[1]+7):

				if(get_val(aventure[2],j,i) == PATH):
					screen.blit(image_path_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == WALL):
					screen.blit(image_wall_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == CITY):
					screen.blit(image_city_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == SHOP):
					screen.blit(image_shop_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == GYM):
					screen.blit(image_gym_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == CENTER):
					screen.blit(image_center_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == HOUSE1):
					screen.blit(image_house1_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == HOUSE2):
					screen.blit(image_house2_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == WILD_GRASS):
					screen.blit(image_wild_grass_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == GRASS):
					screen.blit(image_grass_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_B):
					screen.blit(image_border_b_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_BL):
					screen.blit(image_border_bl_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_BR):
					screen.blit(image_border_br_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_L):
					screen.blit(image_border_l_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_LR):
					screen.blit(image_border_lr_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_LRB):
					screen.blit(image_border_lrb_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_LT):
					screen.blit(image_border_lt_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_LTB):
					screen.blit(image_border_ltb_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_LTR):
					screen.blit(image_border_ltr_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_R):
					screen.blit(image_border_r_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_T):
					screen.blit(image_border_t_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_TB):
					screen.blit(image_border_tb_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_TR):
					screen.blit(image_border_tr_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				elif(get_val(aventure[2],j,i) == BORDER_TRB):
					screen.blit(image_border_trb_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))
				#si c'est une maison
				else:
					screen.blit(images_houses[(get_val(aventure[2],j,i)-DATA_RANGE)//(NB_DETAILS*NB_DETAILS)][(get_val(aventure[2],j,i)-DATA_RANGE)%(NB_DETAILS*NB_DETAILS)], ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))

		#Display tous les joueurs
		for (i,j) in player_coordinates:
			screen.blit(image_player_in, ((i-aventure[0]+6)*CASE_WIDTH_IN,(j-aventure[1]+6)*CASE_HEIGHT_IN))

	#rendu carte
	if(map_display_type == "ZOOMED_OUT"):

		for i in range(get_nb_columns(aventure[3])):
			for j in range(get_nb_lines(aventure[3])):
				if(get_val(aventure[3],j,i) == PATH):
					screen.blit(image_path_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == WALL):
					screen.blit(image_wall_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == CITY):
					screen.blit(image_city_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == HOUSE1):
					screen.blit(image_house1_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == HOUSE2):
					screen.blit(image_house2_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == SHOP):
					screen.blit(image_shop_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == CENTER):
					screen.blit(image_center_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				elif(get_val(aventure[3],j,i) == GYM):
					screen.blit(image_gym_out, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				#pour affichage route sur map générale
				if(get_val(aventure[5],j,i) == 1):
					screen.blit(r1, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 2):
					screen.blit(r2, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 3):
					screen.blit(r3, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 4):
					screen.blit(r4, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 5):
					screen.blit(r5, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 6):
					screen.blit(r6, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 7):
					screen.blit(r7, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 8):
					screen.blit(r8, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 9):
					screen.blit(r9, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 10):
					screen.blit(r10, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 11):
					screen.blit(r11, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 12):
					screen.blit(r12, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 13):
					screen.blit(r13, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 14):
					screen.blit(r14, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 15):
					screen.blit(r15, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 16):
					screen.blit(r16, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 17):
					screen.blit(r17, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 18):
					screen.blit(r18, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 19):
					screen.blit(r19, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 20):
					screen.blit(r20, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 21):
					screen.blit(r21, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 22):
					screen.blit(r22, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 23):
					screen.blit(r23, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 24):
					screen.blit(r24, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 25):
					screen.blit(r25, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 26):
					screen.blit(r26, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))
				if(get_val(aventure[5],j,i) == 27):
					screen.blit(r27, (i*CASE_WIDTH_OUT+x_offset,j*CASE_HEIGHT_OUT+y_offset))

		#intersection
		list_intersection = aventure[4]
		for intersection in list_intersection:
			x_intersection = intersection[0]
			y_intersection = intersection[1]

			screen.blit(image_intersection_out, ((x_intersection)*CASE_WIDTH_OUT+x_offset,(y_intersection)*CASE_HEIGHT_OUT+y_offset))

		#joueur sur a map
		x_joueur = aventure[0]//8
		y_joueur = aventure[1]//8

		screen.blit(image_player_out, (x_joueur*CASE_WIDTH_OUT+x_offset,y_joueur*CASE_HEIGHT_OUT+y_offset))

		screen.blit(image_red_cross_out, image_red_cross_out_rect)

	pygame.display.update()
