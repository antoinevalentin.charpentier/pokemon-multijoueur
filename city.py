class City:
	def __init__(self,x,y,width,height):
		#corner top left
		self.name = ""
		self.x_corner = x
		self.y_corner = y

		self.width = width
		self.height = height

		self.x_output = None
		self.y_output = None
		self.orientation_output = None

		self.nb_houses = 0

	def get_name(self):
		return self.name

	def get_x_corner(self):
		return self.x_corner

	def get_y_corner(self):
		return self.y_corner

	def get_width(self):
		return self.width

	def get_height(self):
		return self.height

	def get_x_output(self):
		return self.x_output

	def get_y_output(self):
		return self.y_output

	def get_orientation_output(self):
		return self.orientation_output

	def get_nb_houses(self):
		return self.nb_houses

	def set_name(self,name):
		self.name = name

	def set_x_output(self,x):
		self.x_output = x

	def set_y_output(self,y):
		self.y_output = y

	def set_orientation_output(self,orientation):
		self.orientation_output = orientation

	def set_nb_houses(self,nb_houses):
		self.nb_houses = nb_houses

	def __repr__(self):
		return "ville : x= "+str(self.x_corner)+", y ="+str(self.y_corner)+", width = "+str(self.width)+", height = "+str(self.height)+", nb_houses = "+str(self.nb_houses)